export class BusinessUnit {
  public ID: number;
  public CompanyCode: string;
  public HighlevelName: string;

  constructor(id: number, companyCode: string, highlevelName: string) {
    this.ID = id;
    this.CompanyCode = companyCode;
    this.HighlevelName = highlevelName;
  }
}
