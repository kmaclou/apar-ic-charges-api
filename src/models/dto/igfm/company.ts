export class Company {
  public ID: number;
  public Code: string;
  public LegalEntityName: string;

  constructor(id: number, code: string, legalEntityName: string) {
    this.ID = id;
    this.Code = code;
    this.LegalEntityName = legalEntityName;
  }
}
