import { Transaction } from './transaction';

import * as moment from 'moment';
import { extendMoment } from 'moment-range';

const { range } = extendMoment(moment);

export class Transactions {
  public items: Transaction[];

  constructor(transactions: Transaction[]) {
    this.items = transactions;
  }

  public getUsing(
    contractID: number,
    transactionTypeID: number,
    documentDate: Date,
    billingYear: number,
    billingMonth: number,
    unitPrice: number,
    quantity: number
  ): Transaction[] {
    return this.items.filter(
      (item) =>
        item.Contract.ID === contractID &&
        item.TransactionType.ID === transactionTypeID &&
        item.DocumentDate === documentDate &&
        item.BillingYear === billingYear &&
        item.BillingMonth === billingMonth &&
        Math.round((item.UnitPrice * item.Quantity + Number.EPSILON) * 100) /
          100 ===
          Math.round((unitPrice * quantity + Number.EPSILON) * 100) / 100
    );
  }
}
