import { TraceLogger } from '../decorators/traceLoggerDecorator';
import { Model as IgfmDbModel } from '../models/db/igfm';
import * as IgfmDtoModel from '../models/dto/igfm';
import { SequelizeUtil } from '../utils/sequelize.util';

import {
  Authorized,
  Body,
  Delete,
  Get,
  InternalServerError,
  JsonController,
  Param,
  Post,
  Put
} from 'routing-controllers';

@JsonController()
export class TransactionController {
  /**
   * @swagger
   * /igfm/transaction:
   *    get:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Obtains the Transactions.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Get('/igfm/transaction')
  //@Authorized('BlockchainAccess')
  public async getTransactions(
    @TraceLogger() logger?
  ): Promise<IgfmDtoModel.Transaction[]> {
    return new Promise<IgfmDtoModel.Transaction[]>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const transactions = (
          await SequelizeUtil.databaseConnection.models.Transaction.findAll({
            include: [
              {
                as: 'Contract',
                model: SequelizeUtil.databaseConnection.models.Contract
              },
              {
                as: 'TaxJurisdictionCountry',
                model: SequelizeUtil.databaseConnection.models.Country
              },
              {
                as: 'TransactionCurrency',
                model: SequelizeUtil.databaseConnection.models.Currency
              },
              {
                as: 'TransactionType',
                model: SequelizeUtil.databaseConnection.models.TransactionType
              }
            ]
          })
        ).map((transaction) => transaction.get({ plain: true }));
        resolve(transactions);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  /**
   * @swagger
   * /igfm/transaction/:id:
   *    get:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Obtains the Transaction corresponding to the specified ID.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Get('/igfm/transaction/:id')
  //@Authorized('BlockchainAccess')
  public async getTransaction(@Param('id') id: string, @TraceLogger() logger) {
    return new Promise(async (resolve, reject) => {
      try {
        if (isNaN(id as any)) {
          throw new Error('The specified ID must be an integer.');
        }
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const transaction = await SequelizeUtil.databaseConnection.models.Transaction.findByPk(
          id
        );
        ///KLM: TO DO
        //).then((rawTransaction) => rawTransaction.get({ plain: true }));
        resolve(transaction);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  /**
   * @swagger
   * /igfm/transaction:
   *    post:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Adds the specified new Transaction.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Post('/igfm/transaction')
  //@Authorized('BlockchainAccess')
  public async addTransaction(
    @Body() transactionToAdd: IgfmDtoModel.Transaction,
    @TraceLogger() logger
  ) {
    return new Promise(async (resolve, reject) => {
      await SequelizeUtil.connect();
      SequelizeUtil.define(IgfmDbModel);
      const sequelizeTransaction = await SequelizeUtil.databaseConnection.transaction();
      try {
        const contracts: IgfmDtoModel.Contracts = await this.getContracts();
        const countries: IgfmDtoModel.Countries = await this.getCountries();
        const currencies: IgfmDtoModel.Currencies = await this.getCurrencies();
        const transactions: IgfmDtoModel.Transactions = new IgfmDtoModel.Transactions(
          await this.getTransactions()
        );
        const transactionTypes: IgfmDtoModel.TransactionTypes = await this.getTransactionTypes();

        const validationErrorMessages: string = transactionToAdd.validateBusinessRules(
          contracts,
          countries,
          currencies,
          transactions,
          transactionTypes
        );

        if (!validationErrorMessages) {
          delete transactionToAdd.ID;
          const rawNewtransaction = await SequelizeUtil.databaseConnection.models.Transaction.create(
            transactionToAdd,
            {
              transaction: sequelizeTransaction
            }
          );
          const newTransaction = (
            await SequelizeUtil.databaseConnection.models.Transaction.findAll({
              include: [
                {
                  as: 'Contract',
                  model: SequelizeUtil.databaseConnection.models.Contract
                },
                {
                  as: 'TaxJurisdictionCountry',
                  model: SequelizeUtil.databaseConnection.models.Country
                },
                {
                  as: 'TransactionCurrency',
                  model: SequelizeUtil.databaseConnection.models.Currency
                },
                {
                  as: 'TransactionType',
                  model: SequelizeUtil.databaseConnection.models.TransactionType
                }
              ],
              transaction: sequelizeTransaction,
              where: {
                //@ts-ignore
                ID: rawNewtransaction.ID
              }
            })
          ).map((transaction) => transaction.get({ plain: true }));
          await sequelizeTransaction.commit();
          if (newTransaction.length === 1) {
            const httpResponseData: IgfmDtoModel.HttpResponseData = new IgfmDtoModel.HttpResponseData(
              IgfmDtoModel.HttpResponseData.REQUEST_APPLICATION_SUCCESS,
              '',
              newTransaction[0]
            );
            resolve(httpResponseData);
          } else {
            throw new Error('New transaction not found in database.');
          }
        } else {
          const httpResponseData: IgfmDtoModel.HttpResponseData = new IgfmDtoModel.HttpResponseData(
            IgfmDtoModel.HttpResponseData.REQUEST_APPLICATION_ERROR,
            validationErrorMessages
          );
          await sequelizeTransaction.rollback();
          resolve(httpResponseData);
        }
      } catch (error) {
        await sequelizeTransaction.rollback();
        reject(new InternalServerError(error));
      }
    });
  }

  /**
   * @swagger
   * /igfm/transaction:
   *    put:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Updates the specified Transaction.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Put('/igfm/transaction')
  //@Authorized('BlockchainAccess')
  public async updateTransaction(
    @Body() transactionToUpdate: IgfmDtoModel.Transaction,
    @TraceLogger() logger
  ) {
    return new Promise(async (resolve, reject) => {
      await SequelizeUtil.connect();
      SequelizeUtil.define(IgfmDbModel);
      const sequelizeTransaction = await SequelizeUtil.databaseConnection.transaction();
      try {
        const contracts: IgfmDtoModel.Contracts = await this.getContracts();
        const countries: IgfmDtoModel.Countries = await this.getCountries();
        const currencies: IgfmDtoModel.Currencies = await this.getCurrencies();
        const transactions: IgfmDtoModel.Transactions = new IgfmDtoModel.Transactions(
          await this.getTransactions()
        );
        const transactionTypes: IgfmDtoModel.TransactionTypes = await this.getTransactionTypes();

        const validationErrorMessages: string = transactionToUpdate.validateBusinessRules(
          contracts,
          countries,
          currencies,
          transactions,
          transactionTypes
        );

        if (!validationErrorMessages) {
          await SequelizeUtil.databaseConnection.models.Transaction.update(
            transactionToUpdate,
            {
              transaction: sequelizeTransaction,
              where: {
                ID: transactionToUpdate.ID
              }
            }
          );
          const updatedTransaction = (
            await SequelizeUtil.databaseConnection.models.Transaction.findAll({
              include: [
                {
                  as: 'Contract',
                  model: SequelizeUtil.databaseConnection.models.Contract
                },
                {
                  as: 'TaxJurisdictionCountry',
                  model: SequelizeUtil.databaseConnection.models.Country
                },
                {
                  as: 'TransactionCurrency',
                  model: SequelizeUtil.databaseConnection.models.Currency
                },
                {
                  as: 'TransactionType',
                  model: SequelizeUtil.databaseConnection.models.TransactionType
                }
              ],
              transaction: sequelizeTransaction,
              where: {
                ID: transactionToUpdate.ID
              }
            })
          ).map((transaction) => transaction.get({ plain: true }));

          await sequelizeTransaction.commit();

          if (updatedTransaction.length === 1) {
            const httpResponseData: IgfmDtoModel.HttpResponseData = new IgfmDtoModel.HttpResponseData(
              IgfmDtoModel.HttpResponseData.REQUEST_APPLICATION_SUCCESS,
              '',
              updatedTransaction[0]
            );
            resolve(httpResponseData);
          } else {
            throw new Error('Updated transaction not found in database.');
          }
        } else {
          const httpResponseData: IgfmDtoModel.HttpResponseData = new IgfmDtoModel.HttpResponseData(
            IgfmDtoModel.HttpResponseData.REQUEST_APPLICATION_ERROR,
            validationErrorMessages
          );
          await sequelizeTransaction.rollback();
          resolve(httpResponseData);
        }
      } catch (error) {
        await sequelizeTransaction.rollback();
        reject(new InternalServerError(error));
      }
    });
  }

  /**
   * @swagger
   * /igfm/transaction/:id:
   *    delete:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Deletes the Transaction corresponding to the specified ID.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Delete('/igfm/transaction/:id')
  //@Authorized('BlockchainAccess')
  public async deleteTransaction(
    @Param('id') id: string,
    @TraceLogger() logger
  ) {
    return new Promise(async (resolve, reject) => {
      try {
        if (isNaN(id as any)) {
          throw new Error('The specified ID must be an integer.');
        }
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        await SequelizeUtil.databaseConnection.models.Transaction.destroy({
          where: {
            ID: id
          }
        });
        resolve(id);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getContracts(): Promise<IgfmDtoModel.Contracts> {
    return new Promise<IgfmDtoModel.Contracts>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const contracts = (
          await SequelizeUtil.databaseConnection.models.Contract.findAll({
            include: [
              {
                as: 'BillableItem',
                model: SequelizeUtil.databaseConnection.models.BillableItem
              },
              {
                as: 'ProviderFinanceArea',
                model:
                  SequelizeUtil.databaseConnection.models.ProviderFinanceArea
              },
              {
                as: 'ProviderProfitCentre',
                model: SequelizeUtil.databaseConnection.models.CostCentre
              },
              {
                as: 'RecipientBusinessUnit',
                model: SequelizeUtil.databaseConnection.models.BusinessUnit
              },
              {
                as: 'RecipientCompany',
                model: SequelizeUtil.databaseConnection.models.Company
              },
              {
                as: 'RecipientCostCentre',
                model: SequelizeUtil.databaseConnection.models.CostCentre
              },
              {
                as: 'RecipientFinanceArea',
                model:
                  SequelizeUtil.databaseConnection.models.ProviderFinanceArea
              }
            ]
          })
        ).map((contract) => contract.get({ plain: true }));

        resolve(new IgfmDtoModel.Contracts(contracts));
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getCountries(): Promise<IgfmDtoModel.Countries> {
    return new Promise<IgfmDtoModel.Countries>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const countries = (
          await SequelizeUtil.databaseConnection.models.Country.findAll()
        ).map((country) => country.get({ plain: true }));
        resolve(new IgfmDtoModel.Countries(countries));
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getCurrencies(): Promise<IgfmDtoModel.Currencies> {
    return new Promise<IgfmDtoModel.Currencies>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const currencies = (
          await SequelizeUtil.databaseConnection.models.Currency.findAll()
        ).map((currency) => currency.get({ plain: true }));
        resolve(new IgfmDtoModel.Currencies(currencies));
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getTransactionTypes(): Promise<IgfmDtoModel.TransactionTypes> {
    return new Promise<IgfmDtoModel.TransactionTypes>(
      async (resolve, reject) => {
        try {
          await SequelizeUtil.connect();
          SequelizeUtil.define(IgfmDbModel);
          const transactionTypes = (
            await SequelizeUtil.databaseConnection.models.TransactionType.findAll()
          ).map((transactionType) => transactionType.get({ plain: true }));
          resolve(new IgfmDtoModel.TransactionTypes(transactionTypes));
        } catch (error) {
          reject(new InternalServerError(error));
        }
      }
    );
  }
}
