import { BusinessUnit } from './business-unit';

export class BusinessUnits {
  public items: BusinessUnit[];

  constructor(businessUnits: BusinessUnit[]) {
    this.items = businessUnits;
  }

  public getUsingId(businessUnitId: number): BusinessUnit | undefined {
    return this.items.find((item) => item.ID === businessUnitId);
  }

  public getUsingBusinessUnitHighlevelName(
    businessUnitHighlevelName: string
  ): BusinessUnit | undefined {
    businessUnitHighlevelName = businessUnitHighlevelName
      .trim()
      .toLocaleUpperCase();
    return this.items.find(
      (item) =>
        item.HighlevelName.trim().toLocaleUpperCase() ===
        businessUnitHighlevelName
    );
  }
}
