import { TraceLogger } from '../decorators/traceLoggerDecorator';
import { Model as IgsmDbModel } from '../models/db/igfm';
import { SequelizeUtil } from '../utils/sequelize.util';

import {
  Authorized,
  Get,
  InternalServerError,
  JsonController
} from 'routing-controllers';

@JsonController()
export class BillableItemCategoryController {
  /**
   * @swagger
   * /igfm/billable-item-category:
   *    get:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Obtains the Billable Service Categories.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Get('/igfm/billable-item-category')
  //@Authorized('BlockchainAccess')
  public async getBillableItemCategories(@TraceLogger() logger) {
    return new Promise(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgsmDbModel);
        const billableItemCategories = (
          await SequelizeUtil.databaseConnection.models.BillableItemCategory.findAll()
        ).map((billableItemCategory) =>
          billableItemCategory.get({ plain: true })
        );
        resolve(billableItemCategories);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }
}
