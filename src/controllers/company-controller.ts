import { TraceLogger } from '../decorators/traceLoggerDecorator';
import { Model as IgfmDbModel } from '../models/db/igfm';
import * as IgfmDtoModel from '../models/dto/igfm';
import { SequelizeUtil } from '../utils/sequelize.util';

import {
  Authorized,
  Get,
  InternalServerError,
  JsonController
} from 'routing-controllers';

@JsonController()
export class CompanyController {
  /**
   * @swagger
   * /igfm/company:
   *    get:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Obtains the Companies.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Get('/igfm/company')
  //@Authorized('BlockchainAccess')
  public async getCompanies(
    @TraceLogger() logger
  ): Promise<IgfmDtoModel.Company[]> {
    return new Promise<IgfmDtoModel.Company[]>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const companies = (
          await SequelizeUtil.databaseConnection.models.Company.findAll()
        ).map((company) => company.get({ plain: true }));
        resolve(companies);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }
}
