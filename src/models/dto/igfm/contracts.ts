import { Contract } from './contract';

import * as moment from 'moment';
import { extendMoment } from 'moment-range';

const { range } = extendMoment(moment);

export class Contracts {
  public items: Contract[];

  constructor(contracts: Contract[]) {
    this.items = contracts;
  }

  public getUsingId(contractId: number): Contract | undefined {
    return this.items.find((item) => item.ID === contractId);
  }

  public getUsing(
    billableItemID: number,
    providerProfitCentreCode: number,
    recipientCostCentreCode: number,
    startDate: Date,
    terminationDate: Date
  ): Contract[] {
    if (startDate && terminationDate) {
      const contractDateRange = range(
        moment(startDate + ' 00:00', 'YYYY-MM-DD HH:mm'),
        moment(terminationDate + ' 23:59', 'YYYY-MM-DD HH:mm')
      );
      return this.items.filter(
        (item) =>
          item.BillableItem.ID === billableItemID &&
          item.ProviderProfitCentre.Code === providerProfitCentreCode &&
          item.RecipientCostCentre.Code === recipientCostCentreCode &&
          range(
            moment(item.StartDate + ' 00:00', 'YYYY-MM-DD HH:mm'),
            moment(item.TerminationDate + ' 23:59', 'YYYY-MM-DD HH:mm')
          ).overlaps(contractDateRange)
      );
    }
  }

  public getUsingReference(
    contractReference: string,
    documentDate: Date
  ): Contract[] {
    if (documentDate) {
      const documentDateRange = range(
        moment(documentDate + ' 00:00', 'YYYY-MM-DD HH:mm'),
        moment(documentDate + ' 23:59', 'YYYY-MM-DD HH:mm')
      );
      contractReference = contractReference.replace(/\s+/g, '+');
      return this.items.filter(
        (item) =>
          (
            item.BillableItem.Name +
            '-' +
            item.ProviderProfitCentre.Code +
            '-' +
            item.RecipientCostCentre.Code
          ).replace(/\s+/g, '+') === contractReference &&
          range(
            moment(item.StartDate + ' 00:00', 'YYYY-MM-DD HH:mm'),
            moment(item.TerminationDate + ' 23:59', 'YYYY-MM-DD HH:mm')
          ).overlaps(documentDateRange)
      );
    }
  }
}
