export class Country {
  public ID: number;
  public IsoAlpha2Code: string;
  public IsoAlpha3Code: string;
  public IsoNumericCode: number;
  public Name: string;
  public OfficialStateName: string;

  constructor(
    id: number,
    isoAlpha2Code: string,
    isoAlpha3Code: string,
    isoNumericCode: number,
    name: string,
    officialStateName: string
  ) {
    this.ID = id;
    this.IsoAlpha2Code = isoAlpha2Code;
    this.IsoAlpha3Code = isoAlpha3Code;
    this.IsoNumericCode = isoNumericCode;
    this.Name = name;
    this.OfficialStateName = officialStateName;
  }
}
