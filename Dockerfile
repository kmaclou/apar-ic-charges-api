# Stage 1 - Create cached node_modules.
# Kudos to https://medium.freecodecamp.org/speed-up-node-re-builds-leveraging-docker-multi-stage-builds-and-save-money-65189a4ab115
# Only rebuild layer if package.json has changed
FROM node:12 as apar-ic-charges-api-node_cache

WORKDIR /cache/
COPY package.json .
COPY .npmrc .

RUN yarn

# Stage 2 - set up build container and run app build
FROM node:12 as apar-ic-charges-api-build

COPY . ./src
WORKDIR /src

# Copy cache from Stage 1
COPY --from=apar-ic-charges-api-node_cache /cache/ .

# Build App
RUN yarn build

# Stage 3 - build deployable container
FROM node:12-slim

WORKDIR /usr/src/service

# Copy files from Stage 2
COPY --from=apar-ic-charges-api-build /src/node_modules node_modules
COPY --from=apar-ic-charges-api-build /src/dist dist

EXPOSE 3006

CMD ["node", "-r", "dotenv/config", "./dist/src/app.js"]