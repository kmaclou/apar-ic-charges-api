export class Currency {
  public ID: number;
  public IsoCode: string;
  public Name: string;

  constructor(id: number, isoCode: string, name: string) {
    this.ID = id;
    this.IsoCode = isoCode;
    this.Name = name;
  }
}
