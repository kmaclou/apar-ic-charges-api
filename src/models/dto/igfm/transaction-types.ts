import { TransactionType } from './transaction-type';

export class TransactionTypes {
  public items: TransactionType[];

  constructor(transactionTypes: TransactionType[]) {
    this.items = transactionTypes;
  }

  public getUsingId(transactionTypeId: number): TransactionType | undefined {
    return this.items.find((item) => item.ID === transactionTypeId);
  }

  public getUsingName(name: string): TransactionType | undefined {
    name = name.trim().toLocaleUpperCase();
    return this.items.find(
      (item) => item.Name.trim().toLocaleUpperCase() === name
    );
  }
}
