import * as moment from 'moment';
import {
  Contract,
  Contracts,
  Countries,
  Country,
  Currencies,
  Currency,
  Transactions,
  TransactionType,
  TransactionTypes
} from '.';

export class Transaction {
  public BillingYear: number;
  public BillingMonth: number;
  public Contract?: Contract;
  public ContractID: number;
  public DoaApprover: string;
  public DocumentDate: Date;
  public ID?: number;
  public PostingDate?: Date;
  public Quantity: number;
  public TaxJurisdictionCountry: Country;
  public TaxJurisdictionCountryID: number;
  public TransactionCurrency: Currency;
  public TransactionCurrencyID: number;
  public TransactionType: TransactionType;
  public TransactionTypeID: number;
  public UnitPrice: number;

  constructor(
    billingYear: number,
    billingMonth: number,
    contractID: number,
    doaApprover: string,
    documentDate: Date,
    id: number,
    postingDate: Date,
    quantity: number,
    taxJurisdictionCountryID: number,
    transactionCurrencyID: number,
    transactionTypeID: number,
    unitPrice: number
  ) {
    this.BillingYear = billingYear;
    this.BillingMonth = billingMonth;
    this.ContractID = contractID;
    this.DoaApprover = doaApprover;
    this.DocumentDate = documentDate;
    this.ID = id;
    this.PostingDate = postingDate;
    this.Quantity = quantity;
    this.TaxJurisdictionCountryID = taxJurisdictionCountryID;
    this.TransactionCurrencyID = transactionCurrencyID;
    this.TransactionTypeID = transactionTypeID;
    this.UnitPrice = unitPrice;
  }

  public get TransactionAmount(): number {
    return (
      Math.round((this.UnitPrice * this.Quantity + Number.EPSILON) * 100) / 100
    );
  }

  public get isBillingYearEmpty(): boolean {
    return !this.BillingYear;
  }

  public get isBillingYearValid(): boolean {
    return this.BillingYear >= 2020 && this.BillingYear <= 2120;
  }

  public get isBillingMonthEmpty(): boolean {
    return !this.BillingMonth;
  }

  public get isBillingMonthValid(): boolean {
    return this.BillingMonth >= 1 && this.BillingMonth <= 12;
  }

  public get isDoaApproverEmpty(): boolean {
    return !this.DoaApprover;
  }

  public get isDoaApproverValid(): boolean {
    var emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    return emailRegex.test(this.DoaApprover);
  }

  public get isDocumentDateEmpty(): boolean {
    return !this.DocumentDate;
  }

  public get isQuantityEmpty(): boolean {
    return !this.Quantity;
  }

  public get isUnitPriceEmpty(): boolean {
    return !this.UnitPrice;
  }

  public validatePropertyValues(): string {
    let errorMessages = '';
    if (this.isBillingYearEmpty) {
      errorMessages += 'Billing-year must be specified! ';
    } else if (!this.isBillingYearValid) {
      errorMessages += 'Billing-year must be in the range 2020 to 2120!';
    }
    if (this.isBillingMonthEmpty) {
      errorMessages += 'Billing-month must be specified! ';
    } else if (!this.isBillingMonthValid) {
      errorMessages += 'Billing-month must be in the range 1 to 12!';
    }
    if (this.isDoaApproverEmpty) {
      errorMessages += 'DOA approver must be specified! ';
    } else if (!this.isDoaApproverValid) {
      errorMessages += 'DOA approver must be a vaid email address!';
    }
    if (this.isDocumentDateEmpty) {
      errorMessages += 'Document-date must be specified! ';
    }
    if (this.isQuantityEmpty) {
      errorMessages += 'Quantity must be specified! ';
    }
    if (this.isUnitPriceEmpty) {
      errorMessages += 'Unit-price must be specified! ';
    }
    return errorMessages;
  }

  public validateBusinessRules(
    contracts: Contracts,
    countries: Countries,
    currencies: Currencies,
    transactions: Transactions,
    transactionTypes: TransactionTypes
  ): string {
    let validationErrorMessages: string = this.validatePropertyValues();

    // If it is a new Transaction:
    if (isNaN(this.ID)) {
      if (
        this.ContractID &&
        this.TransactionTypeID &&
        this.DocumentDate &&
        this.BillingYear &&
        this.BillingMonth &&
        this.UnitPrice &&
        this.Quantity
      ) {
        const existingTransactions: Transaction[] = transactions.getUsing(
          this.ContractID,
          this.TransactionTypeID,
          this.DocumentDate,
          this.BillingYear,
          this.BillingMonth,
          this.UnitPrice,
          this.Quantity
        );

        if (existingTransactions.length) {
          validationErrorMessages +=
            'Transaction(s) already exist for the specified contract, transaction-type, document-date, billing-period and amount! ';
        }
      }
      // else, if it is an existing Transaction:
    } else {
      if (
        this.ContractID &&
        this.TransactionTypeID &&
        this.DocumentDate &&
        this.BillingYear &&
        this.BillingMonth &&
        this.UnitPrice &&
        this.Quantity
      ) {
        const existingTransactions: Transaction[] = transactions.getUsing(
          this.ContractID,
          this.TransactionTypeID,
          this.DocumentDate,
          this.BillingYear,
          this.BillingMonth,
          this.UnitPrice,
          this.Quantity
        );
        if (existingTransactions.length === 1) {
          if (existingTransactions[0].ID !== this.ID) {
            validationErrorMessages +=
              'A transaction for the specified contract, transaction-type, document-date, billing-period and amount! ';
          }
        } else if (existingTransactions.length > 1) {
          validationErrorMessages +=
            'Transactions already exist for the specified contract, transaction-type, document-date, billing-period and amount! ';
        }
      }
    }

    const contract: Contract = contracts.getUsingId(this.ContractID);
    if (!contract) {
      validationErrorMessages += 'Invalid contract ID! ';
    } else {
      this.Contract = contract;
      const billingPeriodDate = `${this.BillingYear}-${this.BillingMonth}-1`;
      if (
        moment(billingPeriodDate, 'YYYY-MM-DD').isBefore(
          moment(this.Contract.StartDate, 'YYYY-MM-DD'),
          'year'
        )
      ) {
        validationErrorMessages +=
          'Billing year cannot be earlier than the contract start date! ';
      } else if (
        moment(billingPeriodDate, 'YYYY-MM-DD').isSame(
          moment(this.Contract.StartDate, 'YYYY-MM-DD'),
          'year'
        )
      ) {
        if (
          moment(billingPeriodDate, 'YYYY-MM-DD').isBefore(
            moment(this.Contract.StartDate, 'YYYY-MM-DD'),
            'month'
          )
        ) {
          validationErrorMessages +=
            'Billing month cannot be earlier than the contract start date! ';
        }
      }

      if (
        moment(billingPeriodDate, 'YYYY-MM-DD').isAfter(
          moment(this.Contract.TerminationDate, 'YYYY-MM-DD'),
          'year'
        )
      ) {
        validationErrorMessages +=
          'Billing year cannot be later than the contract termination date! ';
      } else if (
        moment(billingPeriodDate, 'YYYY-MM-DD').isSame(
          moment(this.Contract.TerminationDate, 'YYYY-MM-DD'),
          'year'
        )
      ) {
        if (
          moment(billingPeriodDate, 'YYYY-MM-DD').isAfter(
            moment(this.Contract.TerminationDate, 'YYYY-MM-DD'),
            'month'
          )
        ) {
          validationErrorMessages +=
            'Billing month cannot be later than the contract termination date! ';
        }
      }

      if (
        moment(this.DocumentDate, 'YYYY-MM-DD').isBefore(
          moment(billingPeriodDate, 'YYYY-MM-DD')
        )
      ) {
        validationErrorMessages +=
          'Document date cannot be earlier than the first day of the billing year and month date! ';
      }
    }

    const taxJurisdictionCountry: Country = countries.getUsingId(
      this.TaxJurisdictionCountryID
    );
    if (!taxJurisdictionCountry) {
      validationErrorMessages += 'Invalid tax jurisdiction country ID! ';
    }

    const transactionCurrency: Currency = currencies.getUsingId(
      this.TransactionCurrencyID
    );
    if (!transactionCurrency) {
      validationErrorMessages += 'Invalid transaction currency ID! ';
    }

    const transactionType: TransactionType = transactionTypes.getUsingId(
      this.TransactionCurrencyID
    );
    if (!transactionType) {
      validationErrorMessages += 'Invalid transaction type ID! ';
    }

    return validationErrorMessages;
  }
}
