import { ProviderFinanceArea } from './provider-finance-area';

export class ProviderFinanceAreas {
  public items: ProviderFinanceArea[];

  constructor(providerFinanceAreas: ProviderFinanceArea[]) {
    this.items = providerFinanceAreas;
  }

  public getUsingId(
    providerFinanceAreaId: number
  ): ProviderFinanceArea | undefined {
    return this.items.find((item) => item.ID === providerFinanceAreaId);
  }

  public getUsingProviderFinanceAreaName(
    providerFinanceAreaName: string
  ): ProviderFinanceArea | undefined {
    providerFinanceAreaName = providerFinanceAreaName
      .trim()
      .toLocaleUpperCase();
    return this.items.find(
      (item) => item.Name.trim().toLocaleUpperCase() === providerFinanceAreaName
    );
  }
}
