import { TraceLogger } from '../decorators/traceLoggerDecorator';
import { Model as IgfmDbModel } from '../models/db/igfm';
import * as IgfmDtoModel from '../models/dto/igfm';
import { SequelizeUtil } from '../utils/sequelize.util';

import {
  Authorized,
  Get,
  InternalServerError,
  JsonController
} from 'routing-controllers';

@JsonController()
export class CurrencyController {
  /**
   * @swagger
   * /igfm/currency:
   *    get:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Obtains the Currencies.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Get('/igfm/currency')
  //@Authorized('BlockchainAccess')
  public async getCurrencies(
    @TraceLogger() logger
  ): Promise<IgfmDtoModel.Currency[]> {
    return new Promise<IgfmDtoModel.Currency[]>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const currencies = (
          await SequelizeUtil.databaseConnection.models.Currency.findAll()
        ).map((currency) => currency.get({ plain: true }));
        resolve(currencies);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }
}
