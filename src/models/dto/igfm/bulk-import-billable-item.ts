import { BillableItemCategories, BillableItems } from '../igfm';

const BATCH_START_TOKEN = 'BATCH_START';
const BATCH_END_TOKEN = 'BATCH_END';
const BATCH_START_END_TOKEN = `${BATCH_START_TOKEN}:${BATCH_END_TOKEN}`;

export class BulkImportBillableItem {
  public gridRowNumber: number;
  public categoryName: string;
  public categoryId: number = 0;
  public name: string;
  public description: string;
  public providerBusinessLines: string;
  public providerFunction: string;
  public providerServiceManager: string;
  public providerFinanceArea: string;
  public importValidationErrorMessages: string;
  public batchGuid: string;
  public batchControlToken: string;
  constructor(
    gridRowNumber: number,
    categoryName: string = '',
    name: string = '',
    description: string = '',
    providerBusinessLines: string = '',
    providerFunction: string = '',
    providerServiceManager: string = '',
    providerFinanceArea: string = '',
    importValidationErrorMessages: string = '',
    batchGuid: string = '',
    batchControlToken: string = '',
    categoryId?: number
  ) {
    this.gridRowNumber = gridRowNumber;
    this.categoryName = categoryName;
    this.name = name;
    this.description = description;
    this.providerBusinessLines = providerBusinessLines;
    this.providerFunction = providerFunction;
    this.providerServiceManager = providerServiceManager;
    this.providerFinanceArea = providerFinanceArea;
    this.importValidationErrorMessages = importValidationErrorMessages;
    this.batchGuid = batchGuid;
    this.batchControlToken = batchControlToken;
    if (categoryId) {
      this.categoryId = categoryId;
    }
  }

  public get isCategoryNameEmpty(): boolean {
    if (
      this.categoryName === undefined ||
      this.categoryName.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isNameEmpty(): boolean {
    if (this.name === undefined || this.name.trim().length === 0) {
      return true;
    } else {
      return false;
    }
  }

  public get isDescriptionEmpty(): boolean {
    if (
      this.description === undefined ||
      this.description.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  get isProviderBusinessLinesEmpty(): boolean {
    if (
      this.providerBusinessLines === undefined ||
      this.providerBusinessLines.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isProviderFunctionEmpty(): boolean {
    if (
      this.providerFunction === undefined ||
      this.providerFunction.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isProviderServiceManagerEmpty(): boolean {
    if (
      this.providerServiceManager === undefined ||
      this.providerServiceManager.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isProviderFinanceAreaEmpty(): boolean {
    if (
      this.providerFinanceArea === undefined ||
      this.providerFinanceArea.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isEmpty(): boolean {
    if (
      this.isCategoryNameEmpty &&
      this.isNameEmpty &&
      this.isDescriptionEmpty &&
      this.isProviderBusinessLinesEmpty &&
      this.isProviderFunctionEmpty &&
      this.isProviderServiceManagerEmpty &&
      this.isProviderFinanceAreaEmpty
    ) {
      return true;
    } else {
      return false;
    }
  }

  public validatePropertyValues(): string {
    let errorMessages = '';
    if (this.isCategoryNameEmpty) {
      errorMessages += 'Category must be specified! ';
    }
    if (this.isNameEmpty) {
      errorMessages += 'Name must be specified! ';
    }
    if (this.isProviderBusinessLinesEmpty) {
      errorMessages += 'Provider Business Lines must be specified! ';
    }
    if (this.isProviderFunctionEmpty) {
      errorMessages += 'Provider Function must be specified! ';
    }
    if (this.isProviderServiceManagerEmpty) {
      errorMessages += 'Provider Service Manager must be specified! ';
    }
    return errorMessages;
  }

  public validateBusinessRules(
    billableItemCategories: BillableItemCategories,
    billableItems: BillableItems
  ): string {
    let validationErrorMessages: string = this.validatePropertyValues();

    const billableItemCategoryID = billableItemCategories.getIdForCategoryName(
      this.categoryName
    );
    if (billableItemCategoryID) {
      this.categoryId = billableItemCategoryID;
    } else {
      validationErrorMessages += 'Invalid category! ';
    }

    if (this.name) {
      const existingBillableItem = billableItems.getUsingName(this.name);
      if (existingBillableItem) {
        validationErrorMessages += 'Name already exists! ';
      }
    }

    return validationErrorMessages;
  }

  public get isFirstItemOfBatch(): boolean {
    if (
      this.batchControlToken === BATCH_START_TOKEN ||
      this.batchControlToken === BATCH_START_END_TOKEN
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isLastItemOfBatch(): boolean {
    if (
      this.batchControlToken === BATCH_END_TOKEN ||
      this.batchControlToken === BATCH_START_END_TOKEN
    ) {
      return true;
    } else {
      return false;
    }
  }
}
