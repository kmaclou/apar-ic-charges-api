import * as moment from 'moment';
import {
  BillableItem,
  BillableItems,
  BusinessUnit,
  BusinessUnits,
  Companies,
  Company,
  Contract,
  Contracts,
  CostCentre,
  CostCentres,
  ProviderFinanceArea,
  ProviderFinanceAreas
} from '.';

const BATCH_START_TOKEN = 'BATCH_START';
const BATCH_END_TOKEN = 'BATCH_END';
const BATCH_START_END_TOKEN = `${BATCH_START_TOKEN}:${BATCH_END_TOKEN}`;

export class BulkUploadContract {
  public gridRowNumber: number;
  public billableItemName: string;
  public billableItemID: number;
  public providerFinanceAreaName: string;
  public providerFinanceAreaID: number;
  public providerProfitCentreCode: string;
  public providerProfitCentreID: number;
  public recipientBusinessUnitHighlevelName: string;
  public recipientBusinessUnitID: number;
  public recipientCompanyCode: string;
  public recipientCompanyID: number;
  public recipientCostCentreCode: string;
  public recipientCostCentreID: number;
  public recipientFinanceAreaName: string;
  public recipientFinanceAreaID: number;
  public startDate: string;
  public terminationDate: string;
  public validationErrorMessages: string;
  public batchGuid: string;
  public batchControlToken: string;
  constructor(
    gridRowNumber: number,
    billableItemName: string = '',
    providerFinanceAreaName: string = '',
    providerProfitCentreCode: string = '',
    recipientBusinessUnitHighlevelName: string = '',
    recipientCompanyCode: string = '',
    recipientCostCentreCode: string = '',
    recipientFinanceAreaName: string = '',
    startDate: string = '',
    terminationDate: string = '',
    validationErrorMessages: string = '',
    batchGuid: string = '',
    batchControlToken: string = '',
    billableItemID?: number,
    providerFinanceAreaID?: number,
    providerProfitCentreID?: number,
    recipientBusinessUnitID?: number,
    recipientCompanyID?: number,
    recipientCostCentreID?: number,
    recipientFinanceAreaID?: number
  ) {
    this.gridRowNumber = gridRowNumber;
    this.billableItemName = billableItemName;
    this.providerFinanceAreaName = providerFinanceAreaName;
    this.providerProfitCentreCode = providerProfitCentreCode;
    this.recipientBusinessUnitHighlevelName = recipientBusinessUnitHighlevelName;
    this.recipientCompanyCode = recipientCompanyCode;
    this.recipientCostCentreCode = recipientCostCentreCode;
    this.recipientFinanceAreaName = recipientFinanceAreaName;
    this.startDate = startDate;
    this.terminationDate = terminationDate;
    this.validationErrorMessages = validationErrorMessages;
    this.batchGuid = batchGuid;
    this.batchControlToken = batchControlToken;
    if (billableItemID) {
      this.billableItemID = billableItemID;
    }
    if (providerFinanceAreaID) {
      this.providerFinanceAreaID = providerFinanceAreaID;
    }
    if (providerProfitCentreID) {
      this.providerProfitCentreID = providerProfitCentreID;
    }
    if (recipientBusinessUnitID) {
      this.recipientBusinessUnitID = recipientBusinessUnitID;
    }
    if (recipientCompanyID) {
      this.recipientCompanyID = recipientCompanyID;
    }
    if (recipientCostCentreID) {
      this.recipientCostCentreID = recipientCostCentreID;
    }
    if (recipientFinanceAreaID) {
      this.recipientFinanceAreaID = recipientFinanceAreaID;
    }
  }

  public get isBillableItemNameEmpty(): boolean {
    if (
      this.billableItemName === undefined ||
      this.billableItemName.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isProviderFinanceAreaNameEmpty(): boolean {
    if (
      this.providerFinanceAreaName === undefined ||
      this.providerFinanceAreaName.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isProviderProfitCentreCodeEmpty(): boolean {
    if (
      this.providerProfitCentreCode === undefined ||
      this.providerProfitCentreCode.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  get isRecipientBusinessUnitHighlevelNameEmpty(): boolean {
    if (
      this.recipientBusinessUnitHighlevelName === undefined ||
      this.recipientBusinessUnitHighlevelName.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isRecipientCompanyCodeEmpty(): boolean {
    if (
      this.recipientCompanyCode === undefined ||
      this.recipientCompanyCode.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isRecipientCostCentreCodeEmpty(): boolean {
    if (
      this.recipientCostCentreCode === undefined ||
      this.recipientCostCentreCode.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isRecipientFinanceAreaNameEmpty(): boolean {
    if (
      this.recipientFinanceAreaName === undefined ||
      this.recipientFinanceAreaName.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isStartDateEmpty(): boolean {
    if (this.startDate === undefined || this.startDate.trim().length === 0) {
      return true;
    } else {
      return false;
    }
  }

  public get isTerminationDateEmpty(): boolean {
    if (
      this.terminationDate === undefined ||
      this.terminationDate.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isEmpty(): boolean {
    if (
      this.isBillableItemNameEmpty &&
      this.isProviderFinanceAreaNameEmpty &&
      this.isProviderProfitCentreCodeEmpty &&
      this.isRecipientBusinessUnitHighlevelNameEmpty &&
      this.isRecipientCompanyCodeEmpty &&
      this.isRecipientCostCentreCodeEmpty &&
      this.isRecipientFinanceAreaNameEmpty &&
      this.isStartDateEmpty &&
      this.isTerminationDateEmpty
    ) {
      return true;
    } else {
      return false;
    }
  }

  public validatePropertyValues(): string {
    let errorMessages = '';
    if (this.isBillableItemNameEmpty) {
      errorMessages += 'Billable-item must be specified! ';
    }
    if (this.isProviderFinanceAreaNameEmpty) {
      errorMessages += 'Provider finance area must be specified! ';
    }
    if (this.isProviderProfitCentreCodeEmpty) {
      errorMessages += 'Provider profit-centre must be specified! ';
    }
    if (this.isRecipientCostCentreCodeEmpty) {
      errorMessages += 'Recipient cost-centre must be specified! ';
    }
    if (this.isRecipientFinanceAreaNameEmpty) {
      errorMessages += 'Recipient finance area must be specified! ';
    }
    if (this.isRecipientCompanyCodeEmpty) {
      errorMessages += 'Recipient company code must be specified! ';
    }
    if (
      this.recipientCompanyCode === '1000' &&
      this.isRecipientBusinessUnitHighlevelNameEmpty
    ) {
      errorMessages +=
        'Recipient business unit must be specified for company 1000! ';
    }
    if (this.isStartDateEmpty) {
      errorMessages += 'Start-date must be specified! ';
    } else {
      const startDate = moment(this.startDate, 'DD-MM-YYYY');
      if (startDate.isValid()) {
        this.startDate = startDate.format('YYYY-MM-DD');
      } else {
        errorMessages +=
          'Start-date is either invalid or is in an invalid format! ';
      }
    }
    if (this.isTerminationDateEmpty) {
      errorMessages += 'Termination-date must be specified! ';
    } else {
      const terminationDate = moment(this.terminationDate, 'DD-MM-YYYY');
      if (terminationDate.isValid()) {
        this.terminationDate = terminationDate.format('YYYY-MM-DD');
      } else {
        errorMessages +=
          'Termination-date is either invalid or is in an invalid format! ';
      }
    }
    return errorMessages;
  }

  public validateBusinessRules(
    billableItems: BillableItems,
    businessUnits: BusinessUnits,
    companies: Companies,
    contracts: Contracts,
    costCentres: CostCentres,
    financeAreas: ProviderFinanceAreas
  ): string {
    let validationErrorMessages: string = this.validatePropertyValues();

    if (!this.isBillableItemNameEmpty) {
      const billableItem: BillableItem = billableItems.getUsingName(
        this.billableItemName
      );
      if (billableItem) {
        this.billableItemID = billableItem.ID;
        if (billableItem.IsDiscontinued) {
          validationErrorMessages += 'Billable-item has been discontinued! ';
        }
      } else {
        validationErrorMessages += 'Invalid billable-item! ';
      }
    }

    if (!this.isProviderFinanceAreaNameEmpty) {
      const providerFinanceArea: ProviderFinanceArea = financeAreas.getUsingProviderFinanceAreaName(
        this.providerFinanceAreaName
      );
      if (providerFinanceArea) {
        this.providerFinanceAreaID = providerFinanceArea.ID;
      } else {
        validationErrorMessages += 'Invalid provider finance-area! ';
      }
    }

    let providerProfitCentre: CostCentre;
    if (!this.isProviderProfitCentreCodeEmpty) {
      providerProfitCentre = costCentres.getUsingCostCentreCode(
        parseInt(this.providerProfitCentreCode, 10)
      );
      if (providerProfitCentre) {
        this.providerProfitCentreID = providerProfitCentre.ID;
      } else {
        validationErrorMessages += 'Invalid provider profit-centre! ';
      }
    }

    let recipientCostCentre: CostCentre;
    if (!this.isRecipientCostCentreCodeEmpty) {
      recipientCostCentre = costCentres.getUsingCostCentreCode(
        parseInt(this.recipientCostCentreCode, 10)
      );
      if (recipientCostCentre) {
        this.recipientCostCentreID = recipientCostCentre.ID;
      } else {
        validationErrorMessages += 'Invalid recipient cost-centre! ';
      }
    }

    if (!this.isRecipientFinanceAreaNameEmpty) {
      const recipientFinanceArea: ProviderFinanceArea = financeAreas.getUsingProviderFinanceAreaName(
        this.recipientFinanceAreaName
      );
      if (recipientFinanceArea) {
        this.recipientFinanceAreaID = recipientFinanceArea.ID;
      } else {
        validationErrorMessages += 'Invalid recipient finance-area! ';
      }
    }

    let recipientCompany: Company;
    if (!this.isRecipientCompanyCodeEmpty) {
      recipientCompany = companies.getUsingCompanyCode(
        this.recipientCompanyCode
      );
      if (recipientCompany) {
        this.recipientCompanyID = recipientCompany.ID;
      } else {
        validationErrorMessages += 'Invalid recipient company code! ';
      }
    }

    let recipientBusinessUnit: BusinessUnit;
    if (!this.isRecipientBusinessUnitHighlevelNameEmpty) {
      recipientBusinessUnit = businessUnits.getUsingBusinessUnitHighlevelName(
        this.recipientBusinessUnitHighlevelName
      );
      if (recipientBusinessUnit) {
        this.recipientBusinessUnitID = recipientBusinessUnit.ID;
      } else {
        validationErrorMessages += 'Invalid recipient business-unit! ';
      }
    }

    if (
      !this.isProviderProfitCentreCodeEmpty &&
      !this.isRecipientCostCentreCodeEmpty
    ) {
      if (
        this.providerProfitCentreCode.trim() ===
        this.recipientCostCentreCode.trim()
      ) {
        validationErrorMessages +=
          'Profit-centre and cost-centre cannot be the same! ';
      } else if (providerProfitCentre && recipientCostCentre) {
        if (
          providerProfitCentre.CompanyCode.trim() ===
          recipientCostCentre.CompanyCode.trim()
        ) {
          validationErrorMessages += `Profit-centre and cost-centre cannot belong to the same company (${providerProfitCentre.CompanyCode.trim()})! `;
        }
      }
    }

    if (
      !this.isProviderFinanceAreaNameEmpty &&
      !this.isRecipientFinanceAreaNameEmpty
    ) {
      if (
        this.providerFinanceAreaName.trim().toLocaleUpperCase() ===
        this.recipientFinanceAreaName.trim().toLocaleUpperCase()
      ) {
        validationErrorMessages += 'Finance areas cannot be the same! ';
      }
    }

    if (
      !this.isRecipientBusinessUnitHighlevelNameEmpty &&
      !this.isRecipientCostCentreCodeEmpty
    ) {
      if (recipientBusinessUnit && recipientCostCentre) {
        if (
          recipientBusinessUnit.HighlevelName.trim().toLocaleUpperCase() !==
          recipientCostCentre.BusinessUnitHighlevelName.trim().toLocaleUpperCase()
        ) {
          validationErrorMessages += `Recipient cost-centre does not belong to the specified business unit (${recipientBusinessUnit.HighlevelName.trim().toLocaleUpperCase()})! `;
        }
      }
    }

    if (
      !this.isRecipientCompanyCodeEmpty &&
      !this.isRecipientBusinessUnitHighlevelNameEmpty
    ) {
      if (recipientCompany && recipientBusinessUnit) {
        if (
          recipientCompany.Code.trim().toLocaleUpperCase() !==
          recipientBusinessUnit.CompanyCode.trim().toLocaleUpperCase()
        ) {
          validationErrorMessages += `Recipient business-unit does not belong to the specified company code (${recipientCompany.Code.trim()})! `;
        }
      }
    }

    if (
      !moment(this.startDate, 'YYYY-MM-DD').isBefore(
        moment(this.terminationDate, 'YYYY-MM-DD')
      )
    ) {
      validationErrorMessages += 'Start date must be before termination date! ';
    }

    if (
      this.billableItemID &&
      this.providerProfitCentreCode &&
      this.recipientCostCentreCode &&
      this.startDate &&
      this.terminationDate
    ) {
      const existingContracts: Contract[] = contracts.getUsing(
        this.billableItemID,
        parseInt(this.providerProfitCentreCode, 10),
        parseInt(this.recipientCostCentreCode, 10),
        //@ts-ignore
        this.startDate.substring(0, 10),
        this.terminationDate.substring(0, 10)
      );
      if (existingContracts.length) {
        validationErrorMessages +=
          'Contract(s) already exist for the specified billable-item, profit/cost-centres and period! ';
      }
    }

    return validationErrorMessages;
  }

  public get isFirstItemOfBatch(): boolean {
    if (
      this.batchControlToken === BATCH_START_TOKEN ||
      this.batchControlToken === BATCH_START_END_TOKEN
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isLastItemOfBatch(): boolean {
    if (
      this.batchControlToken === BATCH_END_TOKEN ||
      this.batchControlToken === BATCH_START_END_TOKEN
    ) {
      return true;
    } else {
      return false;
    }
  }
}
