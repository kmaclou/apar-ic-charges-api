import { BillableItemCategory } from './billable-item-category';

export class BillableItemCategories {
  public items: BillableItemCategory[];

  constructor(billableItemCategories: BillableItemCategory[]) {
    this.items = billableItemCategories;
  }

  public getIdForCategoryName(billableItemCategoryName: string): number {
    billableItemCategoryName = billableItemCategoryName
      .trim()
      .toLocaleUpperCase();

    const billableItemCategory:
      | BillableItemCategory
      | undefined = this.items.find(
      (item) =>
        item.Name.trim().toLocaleUpperCase() === billableItemCategoryName
    );

    return billableItemCategory ? billableItemCategory.ID : 0;
  }
}
