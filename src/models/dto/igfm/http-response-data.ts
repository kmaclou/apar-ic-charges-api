const REQUEST_APPLICATION_ERROR: string = 'REQUEST_APPLICATION_ERROR';
const REQUEST_APPLICATION_SUCCESS: string = 'REQUEST_APPLICATION_SUCCESS';

type Statuses =
  | typeof REQUEST_APPLICATION_SUCCESS
  | typeof REQUEST_APPLICATION_ERROR;

export class HttpResponseData {
  public status: Statuses;
  public message: string;
  public dataValues?: any;

  constructor(status: Statuses, message: string, dataValues?: any) {
    this.status = status;
    this.message = message;
    this.dataValues = dataValues;
  }

  public static get REQUEST_APPLICATION_SUCCESS(): Statuses {
    return REQUEST_APPLICATION_SUCCESS;
  }

  public static get REQUEST_APPLICATION_ERROR(): Statuses {
    return REQUEST_APPLICATION_ERROR;
  }
}
