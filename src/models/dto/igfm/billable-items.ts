import { BillableItem } from './billable-item';

export class BillableItems {
  public items: BillableItem[];

  constructor(billableItems: BillableItem[]) {
    this.items = billableItems;
  }

  public getUsingId(billableItemId: number): BillableItem | undefined {
    return this.items.find((item) => item.ID === billableItemId);
  }

  public getUsingName(billableItemName: string): BillableItem | undefined {
    billableItemName = billableItemName.trim().toLocaleUpperCase();
    return this.items.find(
      (item) => item.Name.trim().toLocaleUpperCase() === billableItemName
    );
  }
}
