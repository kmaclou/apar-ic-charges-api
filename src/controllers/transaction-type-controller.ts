import { TraceLogger } from '../decorators/traceLoggerDecorator';
import { Model as IgfmDbModel } from '../models/db/igfm';
import * as IgfmDtoModel from '../models/dto/igfm';
import { SequelizeUtil } from '../utils/sequelize.util';

import {
  Authorized,
  Get,
  InternalServerError,
  JsonController
} from 'routing-controllers';

@JsonController()
export class TransactionTypeController {
  /**
   * @swagger
   * /igfm/transactionType:
   *    get:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Obtains the Transaction Types.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Get('/igfm/transaction-type')
  //@Authorized('BlockchainAccess')
  public async getTransactionTypes(
    @TraceLogger() logger
  ): Promise<IgfmDtoModel.TransactionType[]> {
    return new Promise<IgfmDtoModel.TransactionType[]>(
      async (resolve, reject) => {
        try {
          await SequelizeUtil.connect();
          SequelizeUtil.define(IgfmDbModel);
          const transactionTypes = (
            await SequelizeUtil.databaseConnection.models.TransactionType.findAll()
          ).map((transactionType) => transactionType.get({ plain: true }));
          resolve(transactionTypes);
        } catch (error) {
          reject(new InternalServerError(error));
        }
      }
    );
  }
}
