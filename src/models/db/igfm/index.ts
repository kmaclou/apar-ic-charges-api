import { DataTypes, Sequelize } from 'sequelize';
const IGFM_SCHEMA: string = '';

export class Model {
  public static define(sequelize: Sequelize) {
    const ProviderFinanceArea = sequelize.define(
      'ProviderFinanceArea',
      {
        ID: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        Name: DataTypes.STRING(80)
      },
      {
        schema: IGFM_SCHEMA,
        timestamps: false
      }
    );

    const BillableItemCategory = sequelize.define(
      'BillableItemCategory',
      {
        ID: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        Name: DataTypes.STRING(80)
      },
      {
        schema: IGFM_SCHEMA,
        timestamps: false
      }
    );

    const BillableItem = sequelize.define(
      'BillableItem',
      {
        BillableItemCategoryID: { type: DataTypes.INTEGER, allowNull: false },
        Description: DataTypes.TEXT,
        ID: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        IsDiscontinued: DataTypes.BOOLEAN,
        Name: DataTypes.STRING(120),
        ProviderBusinessLines: DataTypes.STRING(120),
        ProviderFinanceAreaID: { type: DataTypes.INTEGER, allowNull: false },
        ProviderFunction: DataTypes.STRING(120),
        ProviderServiceManager: DataTypes.STRING(120)
      },
      {
        schema: IGFM_SCHEMA,
        timestamps: false
      }
    );

    ProviderFinanceArea.hasMany(BillableItem, { onDelete: 'NO ACTION' });
    BillableItem.belongsTo(ProviderFinanceArea, {
      as: 'ProviderFinanceArea',
      constraints: true
    });

    BillableItemCategory.hasMany(BillableItem, { onDelete: 'NO ACTION' });
    BillableItem.belongsTo(BillableItemCategory, {
      as: 'BillableItemCategory',
      constraints: true
    });

    const Company = sequelize.define(
      'Company',
      {
        Code: DataTypes.STRING(4),
        ID: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        LegalEntityName: DataTypes.STRING(120)
      },
      {
        schema: IGFM_SCHEMA,
        timestamps: false
      }
    );

    const BusinessUnit = sequelize.define(
      'BusinessUnit',
      {
        CompanyCode: DataTypes.STRING(4),
        HighlevelName: DataTypes.STRING(120),
        ID: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        }
      },
      {
        schema: IGFM_SCHEMA,
        timestamps: false
      }
    );

    const CostCentre = sequelize.define(
      'CostCentre',
      {
        BusinessUnitHighlevelName: DataTypes.STRING(120),
        Code: DataTypes.INTEGER,
        CompanyCode: DataTypes.STRING(4),
        Description: DataTypes.STRING(120),
        ID: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        }
      },
      {
        schema: IGFM_SCHEMA,
        timestamps: false
      }
    );

    const Contract = sequelize.define(
      'Contract',
      {
        BillableItemID: { type: DataTypes.INTEGER, allowNull: false },
        ID: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        ProviderFinanceAreaID: { type: DataTypes.INTEGER, allowNull: false },
        ProviderProfitCentreID: { type: DataTypes.INTEGER, allowNull: false },
        RecipientBusinessUnitID: { type: DataTypes.INTEGER, allowNull: true },
        RecipientCompanyID: { type: DataTypes.INTEGER, allowNull: false },
        RecipientCostCentreID: { type: DataTypes.INTEGER, allowNull: false },
        RecipientFinanceAreaID: { type: DataTypes.INTEGER, allowNull: true },
        StartDate: DataTypes.DATEONLY,
        TerminationDate: DataTypes.DATEONLY
      },
      {
        schema: IGFM_SCHEMA,
        timestamps: false
      }
    );

    BillableItem.hasMany(Contract, {
      foreignKey: {
        allowNull: false
      }
    });
    Contract.belongsTo(BillableItem, {
      as: 'BillableItem',
      constraints: true
    });

    ProviderFinanceArea.hasMany(Contract, {
      foreignKey: 'ProviderFinanceAreaID',
      onDelete: 'NO ACTION'
    });
    Contract.belongsTo(ProviderFinanceArea, {
      as: 'ProviderFinanceArea',
      constraints: true
    });

    CostCentre.hasMany(Contract, {
      foreignKey: 'ProviderProfitCentreID',
      onDelete: 'NO ACTION'
    });
    Contract.belongsTo(CostCentre, {
      as: 'ProviderProfitCentre',
      constraints: true
    });

    BusinessUnit.hasMany(Contract, {
      foreignKey: 'RecipientBusinessUnitID',
      onDelete: 'NO ACTION'
    });
    Contract.belongsTo(BusinessUnit, {
      as: 'RecipientBusinessUnit',
      constraints: true
    });

    Company.hasMany(Contract, {
      foreignKey: 'RecipientCompanyID',
      onDelete: 'NO ACTION'
    });
    Contract.belongsTo(Company, {
      as: 'RecipientCompany',
      constraints: true
    });

    CostCentre.hasMany(Contract, {
      foreignKey: 'RecipientCostCentreID',
      onDelete: 'NO ACTION'
    });
    Contract.belongsTo(CostCentre, {
      as: 'RecipientCostCentre',
      constraints: true
    });

    ProviderFinanceArea.hasMany(Contract, {
      foreignKey: 'RecipientFinanceAreaID',
      onDelete: 'NO ACTION'
    });
    Contract.belongsTo(ProviderFinanceArea, {
      as: 'RecipientFinanceArea',
      constraints: true
    });

    const Country = sequelize.define(
      'Country',
      {
        ID: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        IsoAlpha2Code: { type: DataTypes.STRING(2), allowNull: false },
        IsoAlpha3Code: { type: DataTypes.STRING(3), allowNull: false },
        IsoNumericCode: { type: DataTypes.INTEGER, allowNull: false },
        Name: { type: DataTypes.STRING(80), allowNull: false },
        OfficialStateName: { type: DataTypes.STRING(120), allowNull: false }
      },
      {
        schema: IGFM_SCHEMA,
        timestamps: false
      }
    );

    const Currency = sequelize.define(
      'Currency',
      {
        ID: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        IsoCode: { type: DataTypes.STRING(3), allowNull: false },
        Name: { type: DataTypes.STRING(120), allowNull: false }
      },
      {
        schema: IGFM_SCHEMA,
        timestamps: false
      }
    );

    const SupportingDocument = sequelize.define(
      'SupportingDocument',
      {
        Description: { type: DataTypes.STRING(240), allowNull: false },
        ID: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        TransactionID: { type: DataTypes.INTEGER, allowNull: false },
        Url: { type: DataTypes.STRING(1200), allowNull: false }
      },
      {
        schema: IGFM_SCHEMA,
        timestamps: false
      }
    );

    const TransactionType = sequelize.define(
      'TransactionType',
      {
        ID: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        Name: { type: DataTypes.STRING(120), allowNull: false }
      },
      {
        schema: IGFM_SCHEMA,
        timestamps: false
      }
    );

    const Transaction = sequelize.define(
      'Transaction',
      {
        BillingMonth: { type: DataTypes.INTEGER, allowNull: false },
        BillingYear: { type: DataTypes.INTEGER, allowNull: false },
        ContractID: { type: DataTypes.INTEGER, allowNull: false },
        DoaApprover: { type: DataTypes.STRING(120), allowNull: true },
        DocumentDate: { type: DataTypes.DATEONLY, allowNull: true },
        ID: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        PostingDate: { type: DataTypes.DATEONLY, allowNull: true },
        Quantity: { type: DataTypes.INTEGER, allowNull: false },
        TaxJurisdictionCountryID: { type: DataTypes.INTEGER, allowNull: false },
        TransactionCurrencyID: { type: DataTypes.INTEGER, allowNull: false },
        TransactionTypeID: { type: DataTypes.INTEGER, allowNull: false },
        UnitPrice: { type: DataTypes.DECIMAL, allowNull: false }
      },
      {
        schema: IGFM_SCHEMA,
        timestamps: false
      }
    );

    Contract.hasMany(Transaction, {
      foreignKey: {
        allowNull: false
      }
    });
    Transaction.belongsTo(Contract, {
      as: 'Contract',
      constraints: true
    });

    Country.hasMany(Transaction, {
      foreignKey: 'TaxJurisdictionCountryID',
      onDelete: 'NO ACTION'
    });
    Transaction.belongsTo(Country, {
      as: 'TaxJurisdictionCountry',
      constraints: true
    });

    Currency.hasMany(Transaction, {
      foreignKey: 'TransactionCurrencyID',
      onDelete: 'NO ACTION'
    });
    Transaction.belongsTo(Currency, {
      as: 'TransactionCurrency',
      constraints: true
    });

    Transaction.hasMany(SupportingDocument, {
      foreignKey: {
        allowNull: false
      }
    });
    SupportingDocument.belongsTo(Transaction, {
      as: 'Transaction',
      constraints: true
    });

    TransactionType.hasMany(Transaction, {
      foreignKey: 'TransactionTypeID',
      onDelete: 'NO ACTION'
    });
    Transaction.belongsTo(TransactionType, {
      as: 'TransactionType',
      constraints: true
    });

    const FxRate = sequelize.define(
      'FxRate',
      {
        Date: { type: DataTypes.DATEONLY, allowNull: false },
        ExchangeRate: { type: DataTypes.DECIMAL, allowNull: false },
        FromCurrencyID: { type: DataTypes.INTEGER, allowNull: false },
        ID: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        ToCurrencyID: { type: DataTypes.INTEGER, allowNull: false }
      },
      {
        schema: IGFM_SCHEMA,
        timestamps: false
      }
    );

    Currency.hasMany(FxRate, {
      foreignKey: 'FromCurrencyID',
      onDelete: 'NO ACTION'
    });
    FxRate.belongsTo(Currency, {
      as: 'FromCurrency',
      constraints: true
    });

    Currency.hasMany(FxRate, {
      foreignKey: 'ToCurrencyID',
      onDelete: 'NO ACTION'
    });
    FxRate.belongsTo(Currency, {
      as: 'ToCurrency',
      constraints: true
    });
  }
}
