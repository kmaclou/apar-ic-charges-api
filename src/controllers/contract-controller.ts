import { TraceLogger } from '../decorators/traceLoggerDecorator';
import { Model as IgfmDbModel } from '../models/db/igfm';
import * as IgfmDtoModel from '../models/dto/igfm';
import { SequelizeUtil } from '../utils/sequelize.util';

import {
  Authorized,
  Body,
  Delete,
  Get,
  InternalServerError,
  JsonController,
  Param,
  Post,
  Put
} from 'routing-controllers';

@JsonController()
export class ContractController {
  /**
   * @swagger
   * /igfm/contract:
   *    get:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Obtains the Contracts.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Get('/igfm/contract')
  //@Authorized('BlockchainAccess')
  public async getContracts(
    @TraceLogger() logger?
  ): Promise<IgfmDtoModel.Contract[]> {
    return new Promise<IgfmDtoModel.Contract[]>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const contracts = (
          await SequelizeUtil.databaseConnection.models.Contract.findAll({
            include: [
              {
                as: 'BillableItem',
                model: SequelizeUtil.databaseConnection.models.BillableItem
              },
              {
                as: 'ProviderFinanceArea',
                model:
                  SequelizeUtil.databaseConnection.models.ProviderFinanceArea
              },
              {
                as: 'ProviderProfitCentre',
                model: SequelizeUtil.databaseConnection.models.CostCentre
              },
              {
                as: 'RecipientBusinessUnit',
                model: SequelizeUtil.databaseConnection.models.BusinessUnit
              },
              {
                as: 'RecipientCompany',
                model: SequelizeUtil.databaseConnection.models.Company
              },
              {
                as: 'RecipientCostCentre',
                model: SequelizeUtil.databaseConnection.models.CostCentre
              },
              {
                as: 'RecipientFinanceArea',
                model:
                  SequelizeUtil.databaseConnection.models.ProviderFinanceArea
              }
            ]
          })
        ).map((contract) => contract.get({ plain: true }));
        resolve(contracts);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  /**
   * @swagger
   * /igfm/contract/:id:
   *    get:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Obtains the Contract corresponding to the specified ID.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Get('/igfm/contract/:id')
  //@Authorized('BlockchainAccess')
  public async getContract(@Param('id') id: string, @TraceLogger() logger) {
    return new Promise(async (resolve, reject) => {
      try {
        if (isNaN(id as any)) {
          throw new Error('The specified ID must be an integer.');
        }
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const contract = SequelizeUtil.databaseConnection.models.Contract.findByPk(
          id
        );
        resolve(contract);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  /**
   * @swagger
   * /igfm/contract:
   *    post:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Adds the specified new Contract.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Post('/igfm/contract')
  //@Authorized('BlockchainAccess')
  public async addContract(
    @Body() contractToAdd: IgfmDtoModel.Contract,
    @TraceLogger() logger
  ) {
    return new Promise(async (resolve, reject) => {
      await SequelizeUtil.connect();
      SequelizeUtil.define(IgfmDbModel);
      const sequelizeTransaction = await SequelizeUtil.databaseConnection.transaction();
      try {
        const businessUnits: IgfmDtoModel.BusinessUnits = await this.getBusinessUnits();
        const companies: IgfmDtoModel.Companies = await this.getCompanies();
        const contracts: IgfmDtoModel.Contracts = new IgfmDtoModel.Contracts(
          await this.getContracts()
        );
        const costCentres: IgfmDtoModel.CostCentres = await this.getCostCentres();
        const financeAreas: IgfmDtoModel.ProviderFinanceAreas = await this.getFinanceAreas();
        const billableItems: IgfmDtoModel.BillableItems = await this.getBillableItems();

        const validationErrorMessages: string = contractToAdd.validateBusinessRules(
          billableItems,
          businessUnits,
          companies,
          contracts,
          costCentres,
          financeAreas
        );

        if (!validationErrorMessages) {
          delete contractToAdd.ID;
          const rawNewcontract = await SequelizeUtil.databaseConnection.models.Contract.create(
            contractToAdd,
            {
              transaction: sequelizeTransaction
            }
          );
          const newContract = (
            await SequelizeUtil.databaseConnection.models.Contract.findAll({
              include: [
                {
                  as: 'BillableItem',
                  model: SequelizeUtil.databaseConnection.models.BillableItem
                },
                {
                  as: 'ProviderFinanceArea',
                  model:
                    SequelizeUtil.databaseConnection.models.ProviderFinanceArea
                },
                {
                  as: 'ProviderProfitCentre',
                  model: SequelizeUtil.databaseConnection.models.CostCentre
                },
                {
                  as: 'RecipientBusinessUnit',
                  model: SequelizeUtil.databaseConnection.models.BusinessUnit
                },
                {
                  as: 'RecipientCompany',
                  model: SequelizeUtil.databaseConnection.models.Company
                },
                {
                  as: 'RecipientCostCentre',
                  model: SequelizeUtil.databaseConnection.models.CostCentre
                },
                {
                  as: 'RecipientFinanceArea',
                  model:
                    SequelizeUtil.databaseConnection.models.ProviderFinanceArea
                }
              ],
              transaction: sequelizeTransaction,
              where: {
                //@ts-ignore
                ID: rawNewcontract.ID
              }
            })
          ).map((contract) => contract.get({ plain: true }));

          await sequelizeTransaction.commit();

          if (newContract.length === 1) {
            const httpResponseData: IgfmDtoModel.HttpResponseData = new IgfmDtoModel.HttpResponseData(
              IgfmDtoModel.HttpResponseData.REQUEST_APPLICATION_SUCCESS,
              '',
              newContract[0]
            );
            resolve(httpResponseData);
          } else {
            throw new Error('New contract not found in database.');
          }
        } else {
          const httpResponseData: IgfmDtoModel.HttpResponseData = new IgfmDtoModel.HttpResponseData(
            IgfmDtoModel.HttpResponseData.REQUEST_APPLICATION_ERROR,
            validationErrorMessages
          );
          await sequelizeTransaction.rollback();
          resolve(httpResponseData);
        }
      } catch (error) {
        await sequelizeTransaction.rollback();
        reject(new InternalServerError(error));
      }
    });
  }

  /**
   * @swagger
   * /igfm/contract:
   *    put:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Updates the specified Contract.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Put('/igfm/contract')
  //@Authorized('BlockchainAccess')
  public async updateContract(
    @Body() contractToUpdate: IgfmDtoModel.Contract,
    @TraceLogger() logger
  ) {
    return new Promise(async (resolve, reject) => {
      await SequelizeUtil.connect();
      SequelizeUtil.define(IgfmDbModel);
      const sequelizeTransaction = await SequelizeUtil.databaseConnection.transaction();
      try {
        const businessUnits: IgfmDtoModel.BusinessUnits = await this.getBusinessUnits();
        const companies: IgfmDtoModel.Companies = await this.getCompanies();
        const contracts: IgfmDtoModel.Contracts = new IgfmDtoModel.Contracts(
          await this.getContracts()
        );
        const costCentres: IgfmDtoModel.CostCentres = await this.getCostCentres();
        const financeAreas: IgfmDtoModel.ProviderFinanceAreas = await this.getFinanceAreas();
        const billableItems: IgfmDtoModel.BillableItems = await this.getBillableItems();

        const validationErrorMessages: string = contractToUpdate.validateBusinessRules(
          billableItems,
          businessUnits,
          companies,
          contracts,
          costCentres,
          financeAreas
        );

        if (!validationErrorMessages) {
          await SequelizeUtil.databaseConnection.models.Contract.update(
            contractToUpdate,
            {
              transaction: sequelizeTransaction,
              where: {
                ID: contractToUpdate.ID
              }
            }
          );
          const updatedContract = (
            await SequelizeUtil.databaseConnection.models.Contract.findAll({
              include: [
                {
                  as: 'BillableItem',
                  model: SequelizeUtil.databaseConnection.models.BillableItem
                },
                {
                  as: 'ProviderFinanceArea',
                  model:
                    SequelizeUtil.databaseConnection.models.ProviderFinanceArea
                },
                {
                  as: 'ProviderProfitCentre',
                  model: SequelizeUtil.databaseConnection.models.CostCentre
                },
                {
                  as: 'RecipientBusinessUnit',
                  model: SequelizeUtil.databaseConnection.models.BusinessUnit
                },
                {
                  as: 'RecipientCompany',
                  model: SequelizeUtil.databaseConnection.models.Company
                },
                {
                  as: 'RecipientCostCentre',
                  model: SequelizeUtil.databaseConnection.models.CostCentre
                },
                {
                  as: 'RecipientFinanceArea',
                  model:
                    SequelizeUtil.databaseConnection.models.ProviderFinanceArea
                }
              ],
              transaction: sequelizeTransaction,
              where: {
                ID: contractToUpdate.ID
              }
            })
          ).map((contract) => contract.get({ plain: true }));

          await sequelizeTransaction.commit();

          if (updatedContract.length === 1) {
            const httpResponseData: IgfmDtoModel.HttpResponseData = new IgfmDtoModel.HttpResponseData(
              IgfmDtoModel.HttpResponseData.REQUEST_APPLICATION_SUCCESS,
              '',
              updatedContract[0]
            );
            resolve(httpResponseData);
          } else {
            throw new Error('Updated contract not found in database.');
          }
        } else {
          const httpResponseData: IgfmDtoModel.HttpResponseData = new IgfmDtoModel.HttpResponseData(
            IgfmDtoModel.HttpResponseData.REQUEST_APPLICATION_ERROR,
            validationErrorMessages
          );
          await sequelizeTransaction.rollback();
          resolve(httpResponseData);
        }
      } catch (error) {
        await sequelizeTransaction.rollback();
        reject(new InternalServerError(error));
      }
    });
  }

  /**
   * @swagger
   * /igfm/contract/:id:
   *    delete:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Deletes the Contract corresponding to the specified ID.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Delete('/igfm/contract/:id')
  //@Authorized('BlockchainAccess')
  public async deleteContract(@Param('id') id: string, @TraceLogger() logger) {
    return new Promise(async (resolve, reject) => {
      try {
        if (isNaN(id as any)) {
          throw new Error('The specified ID must be an integer.');
        }
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        await SequelizeUtil.databaseConnection.models.Contract.destroy({
          where: {
            ID: id
          }
        });
        resolve(id);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getBillableItems(): Promise<IgfmDtoModel.BillableItems> {
    return new Promise<IgfmDtoModel.BillableItems>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const billableItems = (
          await SequelizeUtil.databaseConnection.models.BillableItem.findAll({
            include: [
              {
                as: 'BillableItemCategory',
                model:
                  SequelizeUtil.databaseConnection.models.BillableItemCategory
              },
              {
                as: 'ProviderFinanceArea',
                model:
                  SequelizeUtil.databaseConnection.models.ProviderFinanceArea
              }
            ]
          })
        ).map((billableItem) => billableItem.get({ plain: true }));

        resolve(new IgfmDtoModel.BillableItems(billableItems));
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getBusinessUnits(): Promise<IgfmDtoModel.BusinessUnits> {
    return new Promise<IgfmDtoModel.BusinessUnits>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const businessUnits = (
          await SequelizeUtil.databaseConnection.models.BusinessUnit.findAll()
        ).map((businessUnit) => businessUnit.get({ plain: true }));
        resolve(new IgfmDtoModel.BusinessUnits(businessUnits));
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getCompanies(): Promise<IgfmDtoModel.Companies> {
    return new Promise<IgfmDtoModel.Companies>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const companies = (
          await SequelizeUtil.databaseConnection.models.Company.findAll()
        ).map((company) => company.get({ plain: true }));
        resolve(new IgfmDtoModel.Companies(companies));
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getCostCentres(): Promise<IgfmDtoModel.CostCentres> {
    return new Promise<IgfmDtoModel.CostCentres>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const costCentres = (
          await SequelizeUtil.databaseConnection.models.CostCentre.findAll()
        ).map((costCentre) => costCentre.get({ plain: true }));
        resolve(new IgfmDtoModel.CostCentres(costCentres));
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getFinanceAreas(): Promise<IgfmDtoModel.ProviderFinanceAreas> {
    return new Promise<IgfmDtoModel.ProviderFinanceAreas>(
      async (resolve, reject) => {
        try {
          await SequelizeUtil.connect();
          SequelizeUtil.define(IgfmDbModel);
          const providerFinanceAreas = (
            await SequelizeUtil.databaseConnection.models.ProviderFinanceArea.findAll()
          ).map((providerFinanceArea) =>
            providerFinanceArea.get({ plain: true })
          );
          resolve(new IgfmDtoModel.ProviderFinanceAreas(providerFinanceAreas));
        } catch (error) {
          reject(new InternalServerError(error));
        }
      }
    );
  }
}
