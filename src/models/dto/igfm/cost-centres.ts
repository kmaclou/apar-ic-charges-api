import { CostCentre } from './cost-centre';

export class CostCentres {
  public items: CostCentre[];

  constructor(costCentres: CostCentre[]) {
    this.items = costCentres;
  }

  public getUsingId(costCentreId: number): CostCentre | undefined {
    return this.items.find((item) => item.ID === costCentreId);
  }

  public getUsingCostCentreCode(
    costCentreCode: number
  ): CostCentre | undefined {
    return this.items.find((item) => item.Code === costCentreCode);
  }
}
