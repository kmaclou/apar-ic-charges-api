import {
  BillableItemCategory,
  BillableItems,
  ProviderFinanceArea
} from '../igfm';

export class BillableItem {
  public ID?: number;
  public BillableItemCategoryID: number;
  public BillableItemCategory?: BillableItemCategory;
  public Name: string;
  public Description: string;
  public ProviderBusinessLines: string;
  public ProviderFunction: string;
  public ProviderServiceManager: string;
  public IsDiscontinued: boolean;
  public ProviderFinanceAreaID: number;
  public ProviderFinanceArea?: ProviderFinanceArea;

  constructor(
    Name: string,
    Description: string,
    ProviderBusinessLines: string,
    ProviderFunction: string,
    ProviderServiceManager: string,
    IsDiscontinued: boolean,
    BillableItemCategoryID: number,
    ProviderFinanceAreaID: number
  ) {
    this.Name = Name;
    this.Description = Description;
    this.ProviderBusinessLines = ProviderBusinessLines;
    this.ProviderFunction = ProviderFunction;
    this.ProviderServiceManager = ProviderServiceManager;
    this.IsDiscontinued = IsDiscontinued;
    this.BillableItemCategoryID = BillableItemCategoryID;
    this.ProviderFinanceAreaID = ProviderFinanceAreaID;
  }

  public get isNameEmpty(): boolean {
    if (this.Name === undefined || this.Name.trim().length === 0) {
      return true;
    } else {
      return false;
    }
  }

  public get isDescriptionEmpty(): boolean {
    if (
      this.Description === undefined ||
      this.Description.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  get isProviderBusinessLinesEmpty(): boolean {
    if (
      this.ProviderBusinessLines === undefined ||
      this.ProviderBusinessLines.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isProviderFunctionEmpty(): boolean {
    if (
      this.ProviderFunction === undefined ||
      this.ProviderFunction.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isProviderServiceManagerEmpty(): boolean {
    if (
      this.ProviderServiceManager === undefined ||
      this.ProviderServiceManager.trim().length === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isEmpty(): boolean {
    if (
      this.isNameEmpty &&
      this.isDescriptionEmpty &&
      this.isProviderBusinessLinesEmpty &&
      this.isProviderFunctionEmpty &&
      this.isProviderServiceManagerEmpty
    ) {
      return true;
    } else {
      return false;
    }
  }

  public validatePropertyValues(): string {
    let errorMessages = '';
    if (this.isNameEmpty) {
      errorMessages += 'Name must be specified! ';
    }
    if (this.isProviderBusinessLinesEmpty) {
      errorMessages += 'Provider Business Lines must be specified! ';
    }
    if (this.isProviderFunctionEmpty) {
      errorMessages += 'Provider Function must be specified! ';
    }
    if (this.isProviderServiceManagerEmpty) {
      errorMessages += 'Provider Service Manager must be specified! ';
    }
    return errorMessages;
  }

  public validateBusinessRules(billableItems: BillableItems): string {
    let validationErrorMessages: string = this.validatePropertyValues();

    // If it is a new Billable Item:
    if (isNaN(this.ID)) {
      if (this.Name) {
        const existingBillableItem = billableItems.getUsingName(this.Name);
        if (existingBillableItem) {
          validationErrorMessages += 'Name already exists! ';
        }
      }
      // else, if it is an existing Billable Item:
    } else {
      if (this.Name) {
        const existingBillableItem = billableItems.getUsingName(this.Name);
        if (existingBillableItem && existingBillableItem.ID !== this.ID) {
          validationErrorMessages += 'Name already exists! ';
        }
      }
    }

    return validationErrorMessages;
  }
}
