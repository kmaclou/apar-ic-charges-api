import { TraceLogger } from '../decorators/traceLoggerDecorator';
import { Model as IgfmDbModel } from '../models/db/igfm';
import * as IgfmDtoModel from '../models/dto/igfm';
import { SequelizeUtil } from '../utils/sequelize.util';

import {
  Authorized,
  Get,
  InternalServerError,
  JsonController
} from 'routing-controllers';

@JsonController()
export class ProviderFinanceAreaController {
  /**
   * @swagger
   * /igfm/provider-finance-area:
   *    get:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Obtains the Provider Finance Areas.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Get('/igfm/provider-finance-area')
  //@Authorized('BlockchainAccess')
  public async getProviderFinanceAreas(
    @TraceLogger() logger
  ): Promise<IgfmDtoModel.ProviderFinanceArea[]> {
    return new Promise<IgfmDtoModel.ProviderFinanceArea[]>(
      async (resolve, reject) => {
        try {
          await SequelizeUtil.connect();
          SequelizeUtil.define(IgfmDbModel);
          const providerFinanceAreas = (
            await SequelizeUtil.databaseConnection.models.ProviderFinanceArea.findAll()
          ).map((providerFinanceArea) =>
            providerFinanceArea.get({ plain: true })
          );
          resolve(providerFinanceAreas);
        } catch (error) {
          reject(new InternalServerError(error));
        }
      }
    );
  }
  @Get('/igfm/createdb')
  public async createDB(@TraceLogger() logger) {
    return new Promise(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        await SequelizeUtil.databaseConnection.sync({ force: true });
        resolve('tables created');
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }
}
