import { Currency } from './currency';

export class Currencies {
  public items: Currency[];

  constructor(currencies: Currency[]) {
    this.items = currencies;
  }

  public getUsingId(currencyId: number): Currency | undefined {
    return this.items.find((item) => item.ID === currencyId);
  }

  public getUsingIsoCode(isoCode: string): Currency | undefined {
    isoCode = isoCode.trim().toLocaleUpperCase();
    return this.items.find(
      (item) => item.IsoCode.trim().toLocaleUpperCase() === isoCode
    );
  }
}
