import * as IgfmDtoModel from '.';
import { BulkUploadContract } from './bulk-upload-contract';

const BATCH_START_TOKEN = 'BATCH_START';
const BATCH_END_TOKEN = 'BATCH_END';
const BATCH_START_END_TOKEN = `${BATCH_START_TOKEN}:${BATCH_END_TOKEN}`;

export class BulkUploadContracts {
  public items: BulkUploadContract[];
  public batchGuid: string = '';

  constructor(bulkImportContracts: BulkUploadContract[]) {
    if (bulkImportContracts.length) {
      this.batchGuid = bulkImportContracts[0].batchGuid;
    }

    this.items = bulkImportContracts.map(
      (bulkImportContract) =>
        new IgfmDtoModel.BulkUploadContract(
          bulkImportContract.gridRowNumber,
          bulkImportContract.billableItemName,
          bulkImportContract.providerFinanceAreaName,
          bulkImportContract.providerProfitCentreCode,
          bulkImportContract.recipientBusinessUnitHighlevelName,
          bulkImportContract.recipientCompanyCode,
          bulkImportContract.recipientCostCentreCode,
          bulkImportContract.recipientFinanceAreaName,
          bulkImportContract.startDate,
          bulkImportContract.terminationDate,
          '',
          bulkImportContract.batchGuid,
          bulkImportContract.batchControlToken
        )
    );
  }

  public addItems(bulkImportContracts: BulkUploadContract[]) {
    bulkImportContracts.map((bulkImportContract) =>
      this.items.push(
        new IgfmDtoModel.BulkUploadContract(
          bulkImportContract.gridRowNumber,
          bulkImportContract.billableItemName,
          bulkImportContract.providerFinanceAreaName,
          bulkImportContract.providerProfitCentreCode,
          bulkImportContract.recipientBusinessUnitHighlevelName,
          bulkImportContract.recipientCompanyCode,
          bulkImportContract.recipientCostCentreCode,
          bulkImportContract.recipientFinanceAreaName,
          bulkImportContract.startDate,
          bulkImportContract.terminationDate,
          bulkImportContract.validationErrorMessages,
          bulkImportContract.batchGuid,
          bulkImportContract.batchControlToken,
          bulkImportContract.billableItemID,
          bulkImportContract.providerFinanceAreaID,
          bulkImportContract.providerProfitCentreID,
          bulkImportContract.recipientBusinessUnitID,
          bulkImportContract.recipientCompanyID,
          bulkImportContract.recipientCostCentreID,
          bulkImportContract.recipientFinanceAreaID
        )
      )
    );
  }

  public get isEmpty(): boolean {
    return this.items.length ? true : false;
  }

  public get isStartOfNewBatch(): boolean {
    if (this.items.length >= 1) {
      if (
        this.items[0].batchControlToken === BATCH_START_TOKEN ||
        this.items[0].batchControlToken === BATCH_START_END_TOKEN
      ) {
        this.batchGuid = this.items[0].batchGuid; // set the new batch's GUID
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  public get isEndOfCurrentBatch(): boolean {
    if (this.items.length >= 1) {
      if (
        this.items[this.items.length - 1].batchControlToken ===
          BATCH_END_TOKEN ||
        this.items[this.items.length - 1].batchControlToken ===
          BATCH_START_END_TOKEN
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  public hasDuplicateName(bulkImportContract: BulkUploadContract): boolean {
    return false;
  }

  public get validationErrorCount(): number {
    return this.items.filter((item) => item.validationErrorMessages).length;
  }
}
