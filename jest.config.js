// jest.config.js
module.exports = {
    transform: {
      "^.+\\.(js|jsx|ts|tsx)$": "<rootDir>/node_modules/babel-jest",
    },
    testEnvironment: "node",
    collectCoverageFrom: [
      "src/{controllers,middleware,utils}/*.{js,jsx,ts,tsx}",
      "!src/**/*.d.ts"
    ],
    coveragePathIgnorePatterns: [
      "/node_modules/",
      "/dist/",
      "/src/__tests__/"
    ],
    testPathIgnorePatterns: [
      "/node_modules/",
      "/dist/",
      "/src/__tests__/utils"
    ],
    transformIgnorePatterns: [
      "/node_modules/"
    ],  
    setupFiles: [
      "dotenv/config"
    ],
    testTimeout: 10000,
}
