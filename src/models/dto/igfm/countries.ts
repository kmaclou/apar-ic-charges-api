import { Country } from './country';

export class Countries {
  public items: Country[];

  constructor(countries: Country[]) {
    this.items = countries;
  }

  public getUsingId(countryId: number): Country | undefined {
    return this.items.find((item) => item.ID === countryId);
  }

  public getUsingIsoAlpha2Code(isoAlpha2Code: string): Country | undefined {
    isoAlpha2Code = isoAlpha2Code.trim().toLocaleUpperCase();
    return this.items.find(
      (item) => item.IsoAlpha2Code.trim().toLocaleUpperCase() === isoAlpha2Code
    );
  }
}
