import { TraceLogger } from '../decorators/traceLoggerDecorator';
import { Model as IgfmDbModel } from '../models/db/igfm';
import * as IgfmDtoModel from '../models/dto/igfm';
import { SequelizeUtil } from '../utils/sequelize.util';

import {
  Authorized,
  Get,
  InternalServerError,
  JsonController
} from 'routing-controllers';

@JsonController()
export class CompanyController {
  /**
   * @swagger
   * /igfm/cost-centre:
   *    get:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Obtains the Cost Centre.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Get('/igfm/cost-centre')
  //@Authorized('BlockchainAccess')
  public async getCostCentres(
    @TraceLogger() logger
  ): Promise<IgfmDtoModel.CostCentre[]> {
    return new Promise<IgfmDtoModel.CostCentre[]>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const costCentres = (
          await SequelizeUtil.databaseConnection.models.CostCentre.findAll()
        ).map((costCentre) => costCentre.get({ plain: true }));
        resolve(costCentres);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }
}
