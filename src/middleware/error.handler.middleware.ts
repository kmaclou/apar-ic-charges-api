import { LoggerFactory } from '@ravn/utils';
import {
  ExpressErrorMiddlewareInterface,
  HttpError,
  Middleware
} from 'routing-controllers';
import { v4 as uuid } from 'uuid';
import * as winston from 'winston';

@Middleware({ type: 'after' })
export class ErrorHandlerMiddleware implements ExpressErrorMiddlewareInterface {
  private logger: winston.Logger;

  public constructor() {
    this.logger = LoggerFactory.create();
  }

  public error(
    error: Error,
    request: any,
    response: any,
    next: (err: any) => any
  ): void {
    // 1. Create UID
    const genUuid = uuid();

    // 2. Log error to winston
    this.logger.error(`ID: ${genUuid} - Message: ${error.message}`);

    // print validation errors.
    if ((error as any).errors) {
      for (const err of (error as any).errors) {
        this.logger.error(`ID: ${genUuid} - Constraints: ${err.constraints}`);
      }
    }

    // 3. Return only UID reference
    if (error instanceof HttpError) {
      // process dedicated http error.
      response.status(error.httpCode).send({
        message: `Error Reference ID: ${genUuid}`
      });
    } else {
      // fallback: handle any unexpected error as bad request.
      response.status(400).send({
        message: `Error Reference ID: ${genUuid}`
      });
    }

    next(error);
  }
}
