import { TraceLogger } from '../decorators/traceLoggerDecorator';
import { Model as IgsmDbModel } from '../models/db/igfm';
import * as IgfmDtoModel from '../models/dto/igfm';
import { SequelizeUtil } from '../utils/sequelize.util';

import {
  Authorized,
  Body,
  Delete,
  Get,
  InternalServerError,
  JsonController,
  Param,
  Post,
  Put
} from 'routing-controllers';

@JsonController()
export class BillableItemController {
  /**
   * @swagger
   * /igfm/billable-item:
   *    get:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Obtains the Billable Items.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Get('/igfm/billable-item')
  //@Authorized('BlockchainAccess')
  public async getBillableItems(
    @TraceLogger() logger?
  ): Promise<IgfmDtoModel.BillableItem[]> {
    return new Promise<IgfmDtoModel.BillableItem[]>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgsmDbModel);
        const billableItems = (
          await SequelizeUtil.databaseConnection.models.BillableItem.findAll({
            include: [
              {
                as: 'BillableItemCategory',
                model:
                  SequelizeUtil.databaseConnection.models.BillableItemCategory
              },
              {
                as: 'ProviderFinanceArea',
                model:
                  SequelizeUtil.databaseConnection.models.ProviderFinanceArea
              }
            ]
          })
        ).map((billableItem) => billableItem.get({ plain: true }));

        resolve(billableItems);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  /**
   * @swagger
   * /igfm/billable-item/:id:
   *    get:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Obtains the Billable Item corresponding to the specified ID.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Get('/igfm/billable-item/:id')
  //@Authorized('BlockchainAccess')
  public async getBillableItem(@Param('id') id: string, @TraceLogger() logger) {
    return new Promise(async (resolve, reject) => {
      try {
        if (isNaN(id as any)) {
          throw new Error('The specified ID must be an integer.');
        }
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgsmDbModel);
        const billableItem = SequelizeUtil.databaseConnection.models.BillableItem.findByPk(
          id
        );
        resolve(billableItem);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  /**
   * @swagger
   * /igfm/billable-item:
   *    post:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Adds the specified new Billable Item.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Post('/igfm/billable-item')
  //@Authorized('BlockchainAccess')
  public async addBillableItem(
    @Body() newBillableItem: IgfmDtoModel.BillableItem,
    @TraceLogger() logger
  ) {
    return new Promise(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgsmDbModel);

        const billableItemsArray: IgfmDtoModel.BillableItem[] = await this.getBillableItems();
        const billableItems: IgfmDtoModel.BillableItems = new IgfmDtoModel.BillableItems(
          billableItemsArray
        );
        const updateValidationErrorMessages: string = newBillableItem.validateBusinessRules(
          billableItems
        );

        if (!updateValidationErrorMessages) {
          delete newBillableItem.ID;

          const billableItemData = await SequelizeUtil.databaseConnection.models.BillableItem.create(
            newBillableItem
          );
          const addedBillableItem = await billableItemData.get({ plain: true });

          const billableItemCategoryData = await SequelizeUtil.databaseConnection.models.BillableItemCategory.findByPk(
            addedBillableItem.BillableItemCategoryID
          );
          const billableItemCategory = await billableItemCategoryData.get({
            plain: true
          });
          addedBillableItem.BillableItemCategory = billableItemCategory;

          addedBillableItem.ProviderFinanceArea = new IgfmDtoModel.ProviderFinanceArea(
            1,
            'IGSM Finance'
          );

          const httpResponseData: IgfmDtoModel.HttpResponseData = new IgfmDtoModel.HttpResponseData(
            IgfmDtoModel.HttpResponseData.REQUEST_APPLICATION_SUCCESS,
            '',
            addedBillableItem
          );
          resolve(httpResponseData);
        } else {
          const httpResponseData: IgfmDtoModel.HttpResponseData = new IgfmDtoModel.HttpResponseData(
            IgfmDtoModel.HttpResponseData.REQUEST_APPLICATION_ERROR,
            updateValidationErrorMessages
          );
          resolve(httpResponseData);
        }
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  /**
   * @swagger
   * /igfm/billable-item:
   *    put:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Updates the specified Billable Item.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Put('/igfm/billable-item')
  //@Authorized('BlockchainAccess')
  public async updateBillableItem(
    @Body() updatedBillableItem: IgfmDtoModel.BillableItem,
    @TraceLogger() logger
  ) {
    return new Promise(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgsmDbModel);

        const billableItemsArray: IgfmDtoModel.BillableItem[] = await this.getBillableItems();
        const billableItems: IgfmDtoModel.BillableItems = new IgfmDtoModel.BillableItems(
          billableItemsArray
        );
        const updateValidationErrorMessages: string = updatedBillableItem.validateBusinessRules(
          billableItems
        );

        if (!updateValidationErrorMessages) {
          await SequelizeUtil.databaseConnection.models.BillableItem.update(
            updatedBillableItem,
            {
              where: {
                ID: updatedBillableItem.ID
              }
            }
          );
          const httpResponseData: IgfmDtoModel.HttpResponseData = new IgfmDtoModel.HttpResponseData(
            IgfmDtoModel.HttpResponseData.REQUEST_APPLICATION_SUCCESS,
            '',
            updatedBillableItem
          );
          resolve(httpResponseData);
        } else {
          const httpResponseData: IgfmDtoModel.HttpResponseData = new IgfmDtoModel.HttpResponseData(
            IgfmDtoModel.HttpResponseData.REQUEST_APPLICATION_ERROR,
            updateValidationErrorMessages
          );
          resolve(httpResponseData);
        }
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  /**
   * @swagger
   * /igfm/billable-item/:id:
   *    delete:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Deletes the Billable Item corresponding to the specified ID.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Delete('/igfm/billable-item/:id')
  //@Authorized('BlockchainAccess')
  public async deleteBillableItem(
    @Param('id') id: string,
    @TraceLogger() logger
  ) {
    return new Promise(async (resolve, reject) => {
      try {
        if (isNaN(id as any)) {
          throw new Error('The specified ID must be an integer.');
        }
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgsmDbModel);
        await SequelizeUtil.databaseConnection.models.BillableItem.destroy({
          where: {
            ID: id
          }
        });
        resolve(id);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }
}
