import * as moment from 'moment';
import {
  BillableItem,
  BillableItems,
  BusinessUnit,
  BusinessUnits,
  Companies,
  Company,
  Contracts,
  CostCentre,
  CostCentres,
  ProviderFinanceArea,
  ProviderFinanceAreas
} from '../igfm';

export class Contract {
  public BillableItem?: BillableItem;
  public BillableItemID: number;
  public ID?: number;
  public ProviderFinanceArea?: ProviderFinanceArea;
  public ProviderFinanceAreaID: number;
  public ProviderProfitCentre?: CostCentre;
  public ProviderProfitCentreID?: number;
  public ProviderProfitCentreCode?: number;
  public RecipientBusinessUnit?: BusinessUnit;
  public RecipientBusinessUnitID: number;
  public RecipientCompany?: Company;
  public RecipientCompanyID: number;
  public RecipientCostCentre?: CostCentre;
  public RecipientCostCentreID?: number;
  public RecipientCostCentreCode?: number;
  public RecipientFinanceArea?: ProviderFinanceArea;
  public RecipientFinanceAreaID: number;
  public StartDate: Date;
  public TerminationDate: Date;
  constructor(
    id: number,
    billableItemID: number,
    providerFinanceAreaID: number,
    providerProfitCentreID: number | undefined,
    providerProfitCentreCode: number | undefined,
    recipientBusinessUnitID: number,
    recipientCompanyID: number,
    recipientCostCentreID: number | undefined,
    recipientCostCentreCode: number | undefined,
    recipientFinanceAreaID: number,
    startDate: Date,
    terminationDate: Date
  ) {
    this.ID = id;
    this.BillableItemID = billableItemID;
    this.ProviderFinanceAreaID = providerFinanceAreaID;
    if (providerProfitCentreID) {
      this.ProviderProfitCentreID = providerProfitCentreID;
    }
    if (providerProfitCentreCode) {
      this.ProviderProfitCentreCode = providerProfitCentreCode;
    }
    this.RecipientBusinessUnitID = recipientBusinessUnitID;
    this.RecipientCompanyID = recipientCompanyID;
    if (recipientCostCentreID) {
      this.RecipientCostCentreID = recipientCostCentreID;
    }
    if (recipientCostCentreCode) {
      this.RecipientCostCentreCode = recipientCostCentreCode;
    }
    this.RecipientFinanceAreaID = recipientFinanceAreaID;
    this.StartDate = startDate;
    this.TerminationDate = terminationDate;
  }

  public get isProviderProfitCentreCodeEmpty(): boolean {
    return !this.ProviderProfitCentreCode;
  }

  public get isRecipientCostCentreCodeEmpty(): boolean {
    return !this.RecipientCostCentreCode;
  }

  public get isStartDateEmpty(): boolean {
    return !this.StartDate;
  }

  public get isTerminationDateEmpty(): boolean {
    return !this.TerminationDate;
  }

  public validatePropertyValues(): string {
    let errorMessages = '';

    if (this.isProviderProfitCentreCodeEmpty) {
      errorMessages += 'Provider profit-centre must be specified! ';
    }

    if (this.isRecipientCostCentreCodeEmpty) {
      errorMessages += 'Recipient cost-centre must be specified! ';
    }

    if (this.isStartDateEmpty) {
      errorMessages += 'Start-date must be specified! ';
    }
    if (this.isTerminationDateEmpty) {
      errorMessages += 'Termination-date must be specified! ';
    }
    return errorMessages;
  }

  public validateBusinessRules(
    billableItems: BillableItems,
    businessUnits: BusinessUnits,
    companies: Companies,
    contracts: Contracts,
    costCentres: CostCentres,
    financeAreas: ProviderFinanceAreas
  ): string {
    let validationErrorMessages: string = this.validatePropertyValues();

    // If it is a new Contract:
    if (isNaN(this.ID)) {
      if (
        this.BillableItemID &&
        this.ProviderProfitCentreCode &&
        this.RecipientCostCentreCode &&
        this.StartDate &&
        this.TerminationDate
      ) {
        const existingContracts: Contract[] = contracts.getUsing(
          this.BillableItemID,
          //@ts-ignore
          parseInt(this.ProviderProfitCentreCode, 10),
          //@ts-ignore
          parseInt(this.RecipientCostCentreCode, 10),
          this.StartDate,
          this.TerminationDate
        );

        if (existingContracts.length) {
          validationErrorMessages +=
            'Contract(s) already exist for the specified billable-item, profit/cost-centres and period! ';
        }
      }
      // else, if it is an existing Contract:
    } else {
      if (
        this.BillableItemID &&
        this.ProviderProfitCentreCode &&
        this.RecipientCostCentreCode &&
        this.StartDate &&
        this.TerminationDate
      ) {
        const existingContracts: Contract[] = contracts.getUsing(
          this.BillableItemID,
          //@ts-ignore
          parseInt(this.ProviderProfitCentreCode, 10),
          //@ts-ignore
          parseInt(this.RecipientCostCentreCode, 10),
          this.StartDate,
          this.TerminationDate
        );
        if (existingContracts.length === 1) {
          if (existingContracts[0].ID !== this.ID) {
            validationErrorMessages +=
              'A contract for the specified billable-item, profit/cost-centres and period already exists! ';
          }
        } else if (existingContracts.length > 1) {
          validationErrorMessages +=
            'Contracts already exist for the specified billable-item, profit/cost-centres and period! ';
        }
      }
    }

    const billableItem: BillableItem = billableItems.getUsingId(
      this.BillableItemID
    );
    if (billableItem) {
      if (billableItem.IsDiscontinued) {
        validationErrorMessages += 'Billable-item has been discontinued! ';
      }
    } else {
      validationErrorMessages += 'Invalid billable-item! ';
    }

    const providerFinanceArea: ProviderFinanceArea = financeAreas.getUsingId(
      this.ProviderFinanceAreaID
    );
    if (!providerFinanceArea) {
      validationErrorMessages += 'Invalid provider finance-area! ';
    }

    let providerProfitCentre: CostCentre;
    if (!this.isProviderProfitCentreCodeEmpty) {
      providerProfitCentre = costCentres.getUsingCostCentreCode(
        //@ts-ignore
        parseInt(this.ProviderProfitCentreCode, 10)
      );
      if (providerProfitCentre) {
        this.ProviderProfitCentreID = providerProfitCentre.ID;
      } else {
        validationErrorMessages += 'Invalid provider profit-centre! ';
      }
    }

    let recipientCostCentre: CostCentre;
    if (!this.isRecipientCostCentreCodeEmpty) {
      recipientCostCentre = costCentres.getUsingCostCentreCode(
        //@ts-ignore
        parseInt(this.RecipientCostCentreCode, 10)
      );
      if (recipientCostCentre) {
        this.RecipientCostCentreID = recipientCostCentre.ID;
      } else {
        validationErrorMessages += 'Invalid recipient cost-centre! ';
      }
    }

    const recipientFinanceArea: ProviderFinanceArea = financeAreas.getUsingId(
      this.RecipientFinanceAreaID
    );
    if (!recipientFinanceArea) {
      validationErrorMessages += 'Invalid recipient finance-area! ';
    }
    const recipientCompany: Company = companies.getUsingId(
      this.RecipientCompanyID
    );
    if (!recipientCompany) {
      validationErrorMessages += 'Invalid recipient company! ';
    }

    const recipientBusinessUnit: BusinessUnit = businessUnits.getUsingId(
      this.RecipientBusinessUnitID
    );
    if (!recipientBusinessUnit) {
      validationErrorMessages += 'Invalid recipient business-unit! ';
    }

    if (
      !this.isProviderProfitCentreCodeEmpty &&
      !this.isRecipientCostCentreCodeEmpty
    ) {
      if (this.ProviderProfitCentreCode === this.RecipientCostCentreCode) {
        validationErrorMessages +=
          'Profit-centre and cost-centre cannot be the same! ';
      } else if (providerProfitCentre && recipientCostCentre) {
        if (
          providerProfitCentre.CompanyCode.trim() ===
          recipientCostCentre.CompanyCode.trim()
        ) {
          validationErrorMessages += `Profit-centre and cost-centre cannot belong to the same company (${providerProfitCentre.CompanyCode.trim()})! `;
        }
      }
    }

    if (this.ProviderFinanceAreaID === this.RecipientFinanceAreaID) {
      validationErrorMessages += 'Finance areas cannot be the same! ';
    }

    if (recipientBusinessUnit && recipientCostCentre) {
      if (
        recipientBusinessUnit.HighlevelName.trim().toLocaleUpperCase() !==
        recipientCostCentre.BusinessUnitHighlevelName.trim().toLocaleUpperCase()
      ) {
        validationErrorMessages += `Recipient cost-centre does not belong to the specified business unit (${recipientBusinessUnit.HighlevelName.trim().toLocaleUpperCase()})! `;
      }
    }

    if (recipientCompany && recipientBusinessUnit) {
      if (
        !businessUnits.items.filter(
          (businessUnit) =>
            businessUnit.CompanyCode.trim() === recipientCompany.Code.trim() &&
            businessUnit.HighlevelName.trim().toLocaleUpperCase() ===
              recipientBusinessUnit.CompanyCode.trim().toLocaleUpperCase()
        )
      ) {
        validationErrorMessages += `Recipient business-unit does not belong to the specified company code (${recipientCompany.Code.trim()})! `;
      }
    }

    if (
      !moment(this.StartDate, 'YYYY-MM-DD').isBefore(
        moment(this.TerminationDate, 'YYYY-MM-DD')
      )
    ) {
      validationErrorMessages += 'Start date must be before termination date! ';
    }

    return validationErrorMessages;
  }
}
