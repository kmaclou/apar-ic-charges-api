import { TraceLogger } from '../decorators/traceLoggerDecorator';
import { Model as IgfmDbModel } from '../models/db/igfm';
import * as IgfmDtoModel from '../models/dto/igfm';
import { SequelizeUtil } from '../utils/sequelize.util';

import {
  Authorized,
  Body,
  InternalServerError,
  JsonController,
  Param,
  Post
} from 'routing-controllers';

const NodeCache = require('node-cache');

@JsonController()
export class BulkImportBillableItemController {
  private bulkUploadCache;

  constructor() {
    this.bulkUploadCache = new NodeCache({ stdTTL: 3600, checkperiod: 300 });
  }

  /**
   * @swagger
   * /igfm/bulk-import-billable-item:
   *    post:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Imports the specified new Billable Items.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Post('/igfm/bulk-import-billable-item')
  //@Authorized('BlockchainAccess')
  public async importBillableItems(
    @Body() rawBulkImportBillableItems: IgfmDtoModel.BulkImportBillableItem[],
    @TraceLogger() logger
  ) {
    interface ICachedValues {
      billableItemCategories: IgfmDtoModel.BillableItemCategories;
      billableItems: IgfmDtoModel.BillableItems;
      validatedBulkImportBillableItems: IgfmDtoModel.BulkImportBillableItems;
    }

    return new Promise(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);

        const bulkImportBillableItemsResponses: object = {};
        const bulkImportBillableItems = new IgfmDtoModel.BulkImportBillableItems(
          rawBulkImportBillableItems
        );

        let billableItemCategories: IgfmDtoModel.BillableItemCategories;
        let billableItems: IgfmDtoModel.BillableItems;

        // if it is the beginning a new batch upload, then cache the required
        // collections for use by subsequent use portions of the batch:
        if (bulkImportBillableItems.isStartOfNewBatch) {
          billableItemCategories = await this.getBillableItemCategories();
          billableItems = await this.getBillableItems();
          const cacheValuesForNewUploadBatch: ICachedValues = {
            billableItemCategories,
            billableItems,
            validatedBulkImportBillableItems: new IgfmDtoModel.BulkImportBillableItems(
              new Array<IgfmDtoModel.BulkImportBillableItem>()
            )
          };
          this.bulkUploadCache.set(
            bulkImportBillableItems.batchGuid,
            cacheValuesForNewUploadBatch
          );
        }

        // obtain the required collections from the cache:
        const currentUploadCacheValues = this.bulkUploadCache.get(
          bulkImportBillableItems.batchGuid
        );
        billableItemCategories =
          currentUploadCacheValues.billableItemCategories;
        billableItems = currentUploadCacheValues.billableItems;

        //
        bulkImportBillableItems.items.map((bulkImportBillableItem) => {
          const importValidationErrorMessages: string = bulkImportBillableItem.validateBusinessRules(
            billableItemCategories,
            billableItems
          );

          bulkImportBillableItem.importValidationErrorMessages = importValidationErrorMessages;

          bulkImportBillableItemsResponses[
            bulkImportBillableItem.gridRowNumber
          ] = importValidationErrorMessages;
        });

        currentUploadCacheValues.validatedBulkImportBillableItems.addItems(
          bulkImportBillableItems.items
        );

        this.bulkUploadCache.set(
          bulkImportBillableItems.batchGuid,
          currentUploadCacheValues
        );

        if (bulkImportBillableItems.isEndOfCurrentBatch) {
          if (
            !currentUploadCacheValues.validatedBulkImportBillableItems
              .validationErrorCount
          ) {
            const sequelizeTransaction = await SequelizeUtil.databaseConnection.transaction();
            try {
              const createBillableItemsPromises = currentUploadCacheValues.validatedBulkImportBillableItems.items.map(
                (
                  bulkImportBillableItem: IgfmDtoModel.BulkImportBillableItem
                ) => {
                  const newBillableItem: IgfmDtoModel.BillableItem = new IgfmDtoModel.BillableItem(
                    bulkImportBillableItem.name,
                    bulkImportBillableItem.description,
                    bulkImportBillableItem.providerBusinessLines,
                    bulkImportBillableItem.providerFunction,
                    bulkImportBillableItem.providerServiceManager,
                    false,
                    bulkImportBillableItem.categoryId,
                    1
                  );
                  return SequelizeUtil.databaseConnection.models.BillableItem.create(
                    newBillableItem,
                    { transaction: sequelizeTransaction }
                  );
                }
              );
              await Promise.all(createBillableItemsPromises);
              await sequelizeTransaction.commit();
            } catch (error) {
              await sequelizeTransaction.rollback();
              throw new InternalServerError(error);
            }
          }
        }
        resolve(bulkImportBillableItemsResponses);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getBillableItemCategories(): Promise<IgfmDtoModel.BillableItemCategories> {
    return new Promise(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const billableItemCategories = (
          await SequelizeUtil.databaseConnection.models.BillableItemCategory.findAll()
        ).map((billableItemCategory) =>
          billableItemCategory.get({ plain: true })
        );

        resolve(
          new IgfmDtoModel.BillableItemCategories(billableItemCategories)
        );
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getBillableItems(): Promise<IgfmDtoModel.BillableItems> {
    return new Promise<IgfmDtoModel.BillableItems>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const billableItems = (
          await SequelizeUtil.databaseConnection.models.BillableItem.findAll({
            include: [
              {
                as: 'BillableItemCategory',
                model:
                  SequelizeUtil.databaseConnection.models.BillableItemCategory
              },
              {
                as: 'ProviderFinanceArea',
                model:
                  SequelizeUtil.databaseConnection.models.ProviderFinanceArea
              }
            ]
          })
        ).map((billableItem) => billableItem.get({ plain: true }));

        resolve(new IgfmDtoModel.BillableItems(billableItems));
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }
}
