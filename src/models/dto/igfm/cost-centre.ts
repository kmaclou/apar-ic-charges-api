export class CostCentre {
  public ID?: number;
  public CompanyCode: string;
  public BusinessUnitHighlevelName: string;
  public Code: number;
  public Description: string;

  constructor(
    id: number,
    companyCode: string,
    businessUnitHighlevelName: string,
    code: number,
    description: string
  ) {
    this.ID = id;
    this.CompanyCode = companyCode;
    this.BusinessUnitHighlevelName = businessUnitHighlevelName;
    this.Code = code;
    this.Description = description;
  }
}
