import { GatewayFactory, LoggerFactory } from '@ravn/utils';
import * as appInsights from 'applicationinsights';
import * as cors from 'cors';
import * as dotenv from 'dotenv';
import * as express from 'express';
import * as morgan from 'morgan';
import * as passport from 'passport';
import * as PassportAzureAd from 'passport-azure-ad';
import 'reflect-metadata';
import { Action, useContainer, useExpressServer } from 'routing-controllers';
import { Container } from 'typedi';
import * as winston from 'winston';

dotenv.config();

class App {
  private port = process.env.PORT ? process.env.PORT : 3005;

  private logger: winston.Logger = LoggerFactory.create();

  public async run(): Promise<void> {
    this.logger.debug('process.env.AUTH_TENANTID');
    this.logger.debug(process.env.AUTH_TENANTID);
    this.logger.debug('process.env.AUTH_APPID');
    this.logger.debug(process.env.AUTH_APPID);

    const aadConfig = {
      audience: process.env.AUTH_APPID,
      clientID: process.env.AUTH_APPID,
      identityMetadata: `https://login.microsoftonline.com/${process.env.AUTH_TENANTID}/v2.0/.well-known/openid-configuration`,
      issuer: `https://login.microsoftonline.com/${process.env.AUTH_TENANTID}/v2.0`,
      loggingLevel: 'info',
      loggingNoPII: false,
      passReqToCallback: false,
      scope: [
        'BlockchainAccess',
        'openid',
        'profile',
        /*"User.Read",*/ 'email'
      ],
      validateIssuer: true
    };

    // use Bearer passport strategy
    const bearerStrategy = new PassportAzureAd.BearerStrategy(
      aadConfig,
      (token, done) => {
        // Send user info using the second argument
        done(null, { scopes: token.scp }, token);
      }
    );

    // initialize http server
    const app = express();

    // use helmet to secure express.js http headers
    //const helmet = require('helmet');
    //        app.use(helmet());

    // configure content security policy settings (CSP)
    // sources: https://helmetjs.github.io/docs/csp/
    // https://expressjs.com/en/advanced/best-practice-security.html
    // app.use('/api', helmet.contentSecurityPolicy({
    //     directives: {
    //         // tslint:disable: quotemark
    //         connectSrc: ["'self'"],
    //         defaultSrc: ["'none'"],
    //         frameAncestors: ["'self'"],
    //         imgSrc: ["'self'"],
    //         scriptSrc: ["'self'"],
    //         styleSrc: ["'self'"]
    //     }
    // }));

    app.use(passport.initialize());
    passport.use(bearerStrategy);

    // needed for cross resource referencing
    app.use(cors());

    // used for dependency injection
    useContainer(Container);

    Container.set(LoggerFactory, new LoggerFactory());

    // initialize routing
    useExpressServer(app, {
      authorizationChecker: (action: Action, claims: string[]) =>
        new Promise<boolean>((resolve, reject) => {
          passport.authenticate('oauth-bearer', (error, user) => {
            if (error) {
              return reject(error);
            }
            if (!user) {
              return resolve(false);
            }
            action.request.user = user;
            return resolve(true);
          })(action.request, action.response, action.next);
        }),

      controllers: [__dirname + '/controllers/*.js'],
      defaultErrorHandler: false,
      middlewares: [__dirname + '/middleware/*.js'],
      routePrefix: '/api/v1'
    });

    // Log requests
    morgan.token('app-id', function getAppId(req) {
      return req.headers['x-application-id'];
    });

    morgan.token('req-id', function getReqId(req) {
      return req.headers['x-request-id'];
    });

    morgan.token('x-ravn-trace-reqid', function getReqId(req) {
      return req.headers['x-ravn-trace-reqid'];
    });

    morgan.token('x-ravn-trace-sessid', function getReqId(req) {
      return req.headers['x-ravn-trace-sessid'];
    });

    app.use(
      morgan(
        `:method :url :status :req-id :app-id :x-ravn-trace-reqid :x-ravn-trace-sessid`,
        {
          stream: {
            // log all incoming requests
            write: this.requestLogStream
          }
        }
      )
    );

    // set up openapi doc
    const swaggerJsdoc = require('swagger-jsdoc');

    const options = {
      // List of files to be processes. You can also set globs './routes/*.js'
      apis: [__dirname + '/controllers/*.js', __dirname + '/models/*.js'],
      swaggerDefinition: {
        basePath: '/api/v1/',
        consumes: ['application/json'],
        info: {
          contact: {
            name: 'RAVN Team'
          },
          title: 'RAVN AP/AR Inter-Company Charges API',
          version: '1.0.0'
        },
        produces: ['application/json'],
        schemes: ['http', 'https'],
        securityDefinitions: {
          Bearer: {
            in: 'header',
            name: 'Authorization',
            type: 'apiKey'
          }
        }
      }
    };

    const swaggerSpec = swaggerJsdoc(options);

    app.get('/swagger.json', (req: any, res: any) => {
      res.setHeader('Content-Type', 'application/json');
      res.send(swaggerSpec);
    });

    // set up swagger ui
    const swaggerUi = require('swagger-ui-express');
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

    // init Blockchain listener
    // this.initDLTListener(); // Enable when ready

    // start the server
    app.listen(Number(this.port), '0.0.0.0', () => {
      this.logger.info(`App listens on port ${this.port}.`);
    });
  }

  private requestLogStream(message: string) {
    LoggerFactory.create().verbose(message);
  }

  private async initDLTListener() {
    const ibpEnv = process.env.IBP_ENV ? process.env.IBP_ENV : 'dev';
    const connection = await GatewayFactory.create();
    const gateway = connection.gateway;
    const hlfMode = process.env.HLF_MODE ? process.env.HLF_MODE : 'local';
    const contractId = process.env.CONTRACT ? process.env.CONTRACT : 'igfm';
    let network: any;
    if (hlfMode === 'local') {
      network = await gateway.getNetwork('mychannel');
    } else {
      network = await gateway.getNetwork(`igfm-${ibpEnv}`);
    }
    const contract = network.getContract(contractId);

    // Listener Code goes here
  }
}

process.on('unhandledRejection', (error: Error) => {
  LoggerFactory.create().error(
    'Unhandled rejection: ' + (error.stack ? error.stack : error)
  );
});

if (process.env.HLF_MODE !== 'local') {
  appInsights
    .setup(process.env.APP_INSIGHTS_KEY)
    .setAutoDependencyCorrelation(true)
    .setAutoCollectRequests(true)
    .setAutoCollectPerformance(true)
    .setAutoCollectExceptions(true)
    .setAutoCollectDependencies(true)
    .setAutoCollectConsole(true, true)
    .setUseDiskRetryCaching(true)
    .setSendLiveMetrics(true)
    .start();
}

const appSrv = new App();

appSrv.run();
