import { LoggerFactory } from '@ravn/utils';
import { DataTypes, QueryTypes, Sequelize } from 'sequelize';

export class SequelizeUtil {
  public static databaseConnection: Sequelize = null;

  public static async define(model) {
    if (SequelizeUtil.connect()) {
      model.define(SequelizeUtil.databaseConnection);
    }
  }

  public static async selectQuery(selectStatement: string) {
    if (SequelizeUtil.connect()) {
      return await SequelizeUtil.databaseConnection.query(selectStatement, {
        type: QueryTypes.SELECT
      });
    }
  }

  public static async connect() {
    if (SequelizeUtil.databaseConnection === null) {
      await this.connectionSequelize();
    }
  }

  private static async connectionSequelize() {
    const logger = LoggerFactory.create();

    SequelizeUtil.databaseConnection = new Sequelize(
      process.env.AZURESQL_DB,
      process.env.AZURESQL_USER,
      process.env.AZURESQL_PW,
      {
        database: 'igfm',
        dialect: 'mssql',
        dialectOptions: {
          options: {
            debug: {
              data: true,
              packet: true,
              payload: true,
              token: true
            },
            encrypt: true,
            instanceName: process.env.AZURESQL_INSTANCE_NAME
          }
        },
        host: process.env.AZURESQL_HOST,
        pool: {
          idle: 10000,
          max: 5,
          min: 0
        },
        port: parseInt(process.env.AZURESQL_PORT, 10)
      }
    );

    // Override timezone formatting for MSSQL
    DataTypes.DATE.prototype._stringify = function _stringify(date, options) {
      if (date === '' || date === null) {
        return null;
      }
      return this._applyTimezone(date, options).format(
        'YYYY-MM-DD HH:mm:ss.SSS'
      );
    };

    await SequelizeUtil.databaseConnection
      .authenticate()
      .then(async () => {
        logger.info('Connection has been established successfully.');
      })
      .catch((err: any) => {
        logger.error('Unable to connect to the database:' + err);
      });
  }
}
