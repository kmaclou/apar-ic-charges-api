import { TraceLogger } from '../decorators/traceLoggerDecorator';
import { Model as IgfmDbModel } from '../models/db/igfm';
import * as IgfmDtoModel from '../models/dto/igfm';
import { SequelizeUtil } from '../utils/sequelize.util';

import {
  Authorized,
  Get,
  InternalServerError,
  JsonController
} from 'routing-controllers';

@JsonController()
export class BusinessUnitController {
  /**
   * @swagger
   * /igfm/business-unit:
   *    get:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Obtains the Business Units.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Get('/igfm/business-unit')
  //@Authorized('BlockchainAccess')
  public async getBusinessUnits(
    @TraceLogger() logger
  ): Promise<IgfmDtoModel.BusinessUnit[]> {
    return new Promise<IgfmDtoModel.BusinessUnit[]>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const businessUnits = (
          await SequelizeUtil.databaseConnection.models.BusinessUnit.findAll()
        ).map((businessUnit) => businessUnit.get({ plain: true }));
        resolve(businessUnits);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }
}
