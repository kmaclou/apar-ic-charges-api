import { Company } from './company';

export class Companies {
  public items: Company[];

  constructor(companies: Company[]) {
    this.items = companies;
  }

  public getUsingId(companyId: number): Company | undefined {
    return this.items.find((item) => item.ID === companyId);
  }

  public getUsingCompanyCode(companyCode: string): Company | undefined {
    companyCode = companyCode.trim().toLocaleUpperCase();
    return this.items.find(
      (item) => item.Code.trim().toLocaleUpperCase() === companyCode
    );
  }
}
