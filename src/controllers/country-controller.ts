import { TraceLogger } from '../decorators/traceLoggerDecorator';
import { Model as IgfmDbModel } from '../models/db/igfm';
import * as IgfmDtoModel from '../models/dto/igfm';
import { SequelizeUtil } from '../utils/sequelize.util';

import {
  Authorized,
  Get,
  InternalServerError,
  JsonController
} from 'routing-controllers';

@JsonController()
export class CountryController {
  /**
   * @swagger
   * /igfm/country:
   *    get:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Obtains the Countries.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Get('/igfm/country')
  //@Authorized('BlockchainAccess')
  public async getCountries(
    @TraceLogger() logger
  ): Promise<IgfmDtoModel.Country[]> {
    return new Promise<IgfmDtoModel.Country[]>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const countries = (
          await SequelizeUtil.databaseConnection.models.Country.findAll()
        ).map((country) => country.get({ plain: true }));
        resolve(countries);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }
}
