import { LoggerFactory } from '@ravn/utils';
import { createParamDecorator } from 'routing-controllers';

export function TraceLogger() {
  return createParamDecorator({
    value: (action) => {
      const logger = LoggerFactory.create();
      const sessId = action.request.header('x-ravn-trace-sessid');
      const reqId = action.request.header('x-ravn-trace-reqid');
      logger.debug(`TraceLoggerDecorator::${sessId}::${reqId}`);

      const traceLogger = logger.child({
        'x-reqid': reqId,
        'x-sessid': sessId
      });

      return traceLogger;
    }
  });
}
