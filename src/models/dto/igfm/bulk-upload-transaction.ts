import * as moment from 'moment';
import {
  Contract,
  Contracts,
  Countries,
  Country,
  Currencies,
  Currency,
  Transaction,
  Transactions,
  TransactionType,
  TransactionTypes
} from '.';

const BATCH_START_TOKEN = 'BATCH_START';
const BATCH_END_TOKEN = 'BATCH_END';
const BATCH_START_END_TOKEN = `${BATCH_START_TOKEN}:${BATCH_END_TOKEN}`;

export class BulkUploadTransaction {
  public batchGuid: string;
  public batchControlToken: string;
  public billingMonth: string | number;
  public billingYear: string | number;
  public contract?: Contract;
  public contractReference: string;
  public doaApprover: string;
  public documentDate: string | Date;
  public gridRowNumber: number;
  public quantity: string | number;
  public taxJurisdictionCountry?: Country;
  public taxJurisdictionCountryIsoAlpha2Code: string;
  public transactionCurrency?: Currency;
  public transactionCurrencyIsoCode: string;
  public transactionType?: TransactionType;
  public transactionTypeName: string;
  public unitPrice: string | number;
  public validationErrorMessages: string;

  constructor(
    batchGuid: string,
    batchControlToken: string,
    billingMonth: string,
    billingYear: string,
    contractReference: string,
    doaApprover: string,
    documentDate: string,
    gridRowNumber: number,
    quantity: string,
    taxJurisdictionCountryIsoAlpha2Code: string,
    transactionCurrencyIsoCode: string,
    transactionTypeName: string,
    unitPrice: string,
    validationErrorMessages: string = ''
  ) {
    this.batchGuid = batchGuid;
    this.batchControlToken = batchControlToken;
    this.billingYear = billingYear;
    this.billingMonth = billingMonth;
    this.contractReference = contractReference;
    this.doaApprover = doaApprover;
    this.documentDate = documentDate;
    this.gridRowNumber = gridRowNumber;
    this.quantity = quantity;
    this.taxJurisdictionCountryIsoAlpha2Code = taxJurisdictionCountryIsoAlpha2Code;
    this.transactionCurrencyIsoCode = transactionCurrencyIsoCode;
    this.transactionTypeName = transactionTypeName;
    this.unitPrice = unitPrice;
    this.validationErrorMessages = validationErrorMessages;
  }

  public get isBillingYearEmpty(): boolean {
    return !this.billingYear;
  }

  public get isBillingYearValid(): boolean {
    return (
      Number.isInteger(this.billingYear) &&
      parseInt(this.billingYear as string) >= 2020 &&
      parseInt(this.billingYear as string) <= 2120
    );
  }

  public get isBillingMonthEmpty(): boolean {
    return !this.billingMonth;
  }

  public get isBillingMonthValid(): boolean {
    return (
      Number.isInteger(this.billingMonth) &&
      parseInt(this.billingMonth as string) >= 1 &&
      parseInt(this.billingMonth as string) <= 12
    );
  }

  public get isContractReferenceEmpty(): boolean {
    return !this.contractReference;
  }

  public get isDoaApproverEmpty(): boolean {
    return !this.doaApprover;
  }

  public get isDoaApproverValid(): boolean {
    var emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    return emailRegex.test(this.doaApprover);
  }

  public get isDocumentDateEmpty(): boolean {
    return !this.documentDate;
  }

  public get isDocumentDateValid(): boolean {
    return moment(this.documentDate, 'DD-MM-YYYY').isValid();
  }

  public get isQuantityEmpty(): boolean {
    return !this.quantity;
  }

  public get isTaxJurisdictionCountryIsoAlpha3CodeEmpty(): boolean {
    return !this.taxJurisdictionCountryIsoAlpha2Code;
  }

  public get isTransactionCurrencyIsoCodeEmpty(): boolean {
    return !this.transactionCurrencyIsoCode;
  }

  public get isTransactionTypeNameEmpty(): boolean {
    return !this.transactionTypeName;
  }

  public get isUnitPriceEmpty(): boolean {
    return !this.unitPrice;
  }

  public get isEmpty(): boolean {
    if (
      this.isBillingYearEmpty &&
      this.isBillingMonthEmpty &&
      this.isContractReferenceEmpty &&
      this.isDoaApproverEmpty &&
      this.isDocumentDateEmpty &&
      this.isQuantityEmpty &&
      this.isTaxJurisdictionCountryIsoAlpha3CodeEmpty &&
      this.isTransactionCurrencyIsoCodeEmpty &&
      this.isTransactionTypeNameEmpty &&
      this.isUnitPriceEmpty
    ) {
      return true;
    } else {
      return false;
    }
  }

  public validatePropertyValues(): string {
    let errorMessages = '';
    if (this.isContractReferenceEmpty) {
      errorMessages += 'Contract reference must be specified! ';
    }
    if (this.isTransactionTypeNameEmpty) {
      errorMessages += 'Transaction type area must be specified! ';
    }
    if (this.isBillingYearEmpty) {
      errorMessages += 'Billing year must be specified! ';
    } else if (!this.isBillingYearValid) {
      errorMessages += 'Billing year must be in the range 2020 to 2120! ';
    } else {
      this.billingYear = parseInt(this.billingYear as string);
    }
    if (this.isBillingMonthEmpty) {
      errorMessages += 'Billing month must be specified! ';
    } else if (!this.isBillingMonthValid) {
      errorMessages += 'Billing month must be in the range 1 to 12! ';
    } else {
      this.billingMonth = parseInt(this.billingMonth as string);
    }
    if (this.isTransactionCurrencyIsoCodeEmpty) {
      errorMessages += 'Transaction currency code must be specified! ';
    }
    if (this.isUnitPriceEmpty) {
      errorMessages += 'Unit price must be specified! ';
    } else if (Number.isNaN(this.unitPrice)) {
      errorMessages += 'Unit price must be an number! ';
    } else {
      this.unitPrice = parseFloat(this.unitPrice as string);
    }
    if (this.isQuantityEmpty) {
      errorMessages += 'Quantity must be specified! ';
    } else if (!Number.isInteger(this.quantity)) {
      errorMessages += 'Quantity must be an integer! ';
    }
    if (this.isDocumentDateEmpty) {
      errorMessages += 'Document date must be specified! ';
    } else if (!this.isDocumentDateValid) {
      errorMessages +=
        'Document date must be specified in the format, DD-MM-YYYY! ';
    } else {
      this.documentDate = moment(this.documentDate, 'DD-MM-YYYY').format(
        'YYYY-MM-DD'
      );
    }
    if (this.isTaxJurisdictionCountryIsoAlpha3CodeEmpty) {
      errorMessages +=
        'Service consumption tax jurisdiction country must be specified! ';
    }
    if (this.isDoaApproverEmpty) {
      errorMessages += 'DOA approver must be specified! ';
    } else if (!this.isDoaApproverValid) {
      errorMessages += 'DOA approver must be a vaid email address!';
    }

    return errorMessages;
  }

  public validateBusinessRules(
    contracts: Contracts,
    countries: Countries,
    currencies: Currencies,
    transactions: Transactions,
    transactionTypes: TransactionTypes
  ): string {
    let validationErrorMessages: string = this.validatePropertyValues();

    if (!this.isContractReferenceEmpty && !this.isDocumentDateEmpty) {
      const contractsWithReference: Contract[] = contracts.getUsingReference(
        this.contractReference,
        this.documentDate as Date
      );
      if (contractsWithReference.length === 0) {
        validationErrorMessages +=
          'No contract corresponding to the reference was found! ';
      } else if (contractsWithReference.length === 1) {
        this.contract = contractsWithReference[0];
      } else if (contractsWithReference.length > 1) {
        validationErrorMessages +=
          'More than one contract corresponding to the reference was found! ';
      }
    }

    if (!this.isTransactionTypeNameEmpty) {
      this.transactionType = transactionTypes.getUsingName(
        this.transactionTypeName
      );
      if (!this.transactionType) {
        validationErrorMessages += 'Invalid transaction type! ';
      }
    }

    if (!this.isTransactionCurrencyIsoCodeEmpty) {
      this.transactionCurrency = currencies.getUsingIsoCode(
        this.transactionCurrencyIsoCode
      );
      if (!this.transactionCurrency) {
        validationErrorMessages += 'Invalid transaction currency ISO code! ';
      }
    }

    if (this.quantity < 1 || this.quantity > 9999999) {
      validationErrorMessages += 'Quantity must be in the range 1 to 9999999! ';
    }

    if (this.unitPrice < 0.01 || this.unitPrice > 99999999.99) {
      validationErrorMessages +=
        'Unit price must be in the range 0.01 to 99999999.99! ';
    }

    if (!this.isTaxJurisdictionCountryIsoAlpha3CodeEmpty) {
      this.taxJurisdictionCountry = countries.getUsingIsoAlpha2Code(
        this.taxJurisdictionCountryIsoAlpha2Code
      );
      if (!this.taxJurisdictionCountry) {
        validationErrorMessages +=
          'Invalid country ISO code specified for service consumption tax jurisdiction! ';
      }
    }

    if (
      !this.isDocumentDateEmpty &&
      this.isDocumentDateValid &&
      !this.isBillingYearEmpty &&
      this.isBillingYearValid &&
      this.isBillingMonthEmpty &&
      !this.isBillingMonthValid
    ) {
      const firstDayOfBillingPeriod: string =
        this.billingYear + '-' + this.billingMonth + '-01';
      if (
        !moment(this.documentDate, 'YYYY-MM-DD').isBefore(
          moment(firstDayOfBillingPeriod, 'YYYY-MM-DD')
        )
      ) {
        validationErrorMessages +=
          "Document date cannot be before the first day of the billing period's month! ";
      }
    }

    if (
      this.contract &&
      this.transactionType &&
      this.documentDate &&
      this.billingYear &&
      this.billingMonth &&
      this.unitPrice &&
      this.quantity
    ) {
      const existingTransactions: Transaction[] = transactions.getUsing(
        this.contract.ID,
        this.transactionType.ID,
        this.documentDate as Date,
        this.billingYear as number,
        this.billingMonth as number,
        this.unitPrice as number,
        this.quantity as number
      );
      if (existingTransactions.length) {
        validationErrorMessages +=
          'Transaction(s) already exist for the specified contract, transaction-type, document date, billing period and amount! ';
      }
    }

    return validationErrorMessages;
  }

  public get isFirstItemOfBatch(): boolean {
    if (
      this.batchControlToken === BATCH_START_TOKEN ||
      this.batchControlToken === BATCH_START_END_TOKEN
    ) {
      return true;
    } else {
      return false;
    }
  }

  public get isLastItemOfBatch(): boolean {
    if (
      this.batchControlToken === BATCH_END_TOKEN ||
      this.batchControlToken === BATCH_START_END_TOKEN
    ) {
      return true;
    } else {
      return false;
    }
  }
}
