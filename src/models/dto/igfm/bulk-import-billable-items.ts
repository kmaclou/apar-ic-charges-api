import * as IgfmDtoModel from '../../../models/dto/igfm';
import { BulkImportBillableItem } from './bulk-import-billable-item';

const BATCH_START_TOKEN = 'BATCH_START';
const BATCH_END_TOKEN = 'BATCH_END';
const BATCH_START_END_TOKEN = `${BATCH_START_TOKEN}:${BATCH_END_TOKEN}`;

export class BulkImportBillableItems {
  public items: BulkImportBillableItem[];
  public batchGuid: string = '';

  constructor(bulkImportBillableItems: BulkImportBillableItem[]) {
    if (bulkImportBillableItems.length) {
      this.batchGuid = bulkImportBillableItems[0].batchGuid;
    }

    this.items = bulkImportBillableItems.map(
      (bulkImportBillableItem) =>
        new IgfmDtoModel.BulkImportBillableItem(
          bulkImportBillableItem.gridRowNumber,
          bulkImportBillableItem.categoryName,
          bulkImportBillableItem.name,
          bulkImportBillableItem.description,
          bulkImportBillableItem.providerBusinessLines,
          bulkImportBillableItem.providerFunction,
          bulkImportBillableItem.providerServiceManager,
          bulkImportBillableItem.providerFinanceArea,
          '',
          bulkImportBillableItem.batchGuid,
          bulkImportBillableItem.batchControlToken
        )
    );
  }

  public addItems(bulkImportBillableItems: BulkImportBillableItem[]) {
    bulkImportBillableItems.map((bulkImportBillableItem) =>
      this.items.push(
        new IgfmDtoModel.BulkImportBillableItem(
          bulkImportBillableItem.gridRowNumber,
          bulkImportBillableItem.categoryName,
          bulkImportBillableItem.name,
          bulkImportBillableItem.description,
          bulkImportBillableItem.providerBusinessLines,
          bulkImportBillableItem.providerFunction,
          bulkImportBillableItem.providerServiceManager,
          bulkImportBillableItem.providerFinanceArea,
          bulkImportBillableItem.importValidationErrorMessages,
          bulkImportBillableItem.batchGuid,
          bulkImportBillableItem.batchControlToken,
          bulkImportBillableItem.categoryId
        )
      )
    );
  }

  public get isEmpty(): boolean {
    return this.items.length ? true : false;
  }

  public get isStartOfNewBatch(): boolean {
    if (this.items.length >= 1) {
      if (
        this.items[0].batchControlToken === BATCH_START_TOKEN ||
        this.items[0].batchControlToken === BATCH_START_END_TOKEN
      ) {
        this.batchGuid = this.items[0].batchGuid; // set the new batch's GUID
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  public get isEndOfCurrentBatch(): boolean {
    if (this.items.length >= 1) {
      if (
        this.items[this.items.length - 1].batchControlToken ===
          BATCH_END_TOKEN ||
        this.items[this.items.length - 1].batchControlToken ===
          BATCH_START_END_TOKEN
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  public hasDuplicateName(
    bulkImportBillableItem: BulkImportBillableItem
  ): boolean {
    const bulkImportBillableItemName: string = bulkImportBillableItem.name
      .trim()
      .toLocaleUpperCase();

    return this.items.find(
      (item) =>
        item.name.trim().toLocaleUpperCase() === bulkImportBillableItemName &&
        item !== bulkImportBillableItem
    )
      ? true
      : false;
  }

  public get validationErrorCount(): number {
    return this.items.filter((item) => item.importValidationErrorMessages)
      .length;
  }
}
