import { TraceLogger } from '../decorators/traceLoggerDecorator';
import { Model as IgfmDbModel } from '../models/db/igfm';
import * as IgfmDtoModel from '../models/dto/igfm';
import { SequelizeUtil } from '../utils/sequelize.util';

import * as moment from 'moment';

import {
  Authorized,
  Body,
  InternalServerError,
  JsonController,
  Param,
  Post
} from 'routing-controllers';

const NodeCache = require('node-cache');

@JsonController()
export class BulkUploadContractController {
  private bulkUploadCache;

  constructor() {
    this.bulkUploadCache = new NodeCache({ stdTTL: 3600, checkperiod: 300 });
  }

  /**
   * @swagger
   * /igfm/bulk-upload-contract:
   *    post:
   *      security:
   *          - Bearer: []
   *      tags:
   *          - IGSM
   *      description: Bulk-adds the specified uploaded new Contracts.
   *      responses:
   *          200:
   *              description: Status Buffer
   */
  @Post('/igfm/bulk-upload-contract')
  //@Authorized('BlockchainAccess')
  public async bulkUploadContracts(
    @Body() rawBulkUploadContracts: IgfmDtoModel.BulkUploadContract[],
    @TraceLogger() logger
  ) {
    interface ICachedValues {
      billableItems: IgfmDtoModel.BillableItems;
      businessUnits: IgfmDtoModel.BusinessUnits;
      companies: IgfmDtoModel.Companies;
      contracts: IgfmDtoModel.Contracts;
      costCentres: IgfmDtoModel.CostCentres;
      financeAreas: IgfmDtoModel.ProviderFinanceAreas;
      validatedBulkUploadContracts: IgfmDtoModel.BulkUploadContracts;
    }

    return new Promise(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);

        const bulkUploadContractsResponses: object = {};
        const bulkUploadContracts = new IgfmDtoModel.BulkUploadContracts(
          rawBulkUploadContracts
        );

        let billableItems: IgfmDtoModel.BillableItems;
        let businessUnits: IgfmDtoModel.BusinessUnits;
        let companies: IgfmDtoModel.Companies;
        let contracts: IgfmDtoModel.Contracts;
        let costCentres: IgfmDtoModel.CostCentres;
        let financeAreas: IgfmDtoModel.ProviderFinanceAreas;

        // if it is the beginning a new batch upload, then cache the required
        // collections for use by subsequent use portions of the batch:
        if (bulkUploadContracts.isStartOfNewBatch) {
          billableItems = await this.getBillableItems();
          businessUnits = await this.getBusinessUnits();
          companies = await this.getCompanies();
          contracts = await this.getContracts();
          costCentres = await this.getCostCentres();
          financeAreas = await this.getFinanceAreas();
          const cacheValuesForNewUpload: ICachedValues = {
            billableItems,
            businessUnits,
            companies,
            contracts,
            costCentres,
            financeAreas,
            validatedBulkUploadContracts: new IgfmDtoModel.BulkUploadContracts(
              new Array<IgfmDtoModel.BulkUploadContract>()
            )
          };
          this.bulkUploadCache.set(
            bulkUploadContracts.batchGuid,
            cacheValuesForNewUpload
          );
        }

        // obtain the required collections from the cache:
        const currentUploadCacheValues = this.bulkUploadCache.get(
          bulkUploadContracts.batchGuid
        );
        billableItems = currentUploadCacheValues.billableItems;
        businessUnits = currentUploadCacheValues.businessUnits;
        companies = currentUploadCacheValues.companies;
        contracts = currentUploadCacheValues.contracts;
        costCentres = currentUploadCacheValues.costCentres;
        financeAreas = currentUploadCacheValues.financeAreas;

        //
        bulkUploadContracts.items.map((bulkUploadContract) => {
          const bulkUploadValidationErrors: string = bulkUploadContract.validateBusinessRules(
            billableItems,
            businessUnits,
            companies,
            contracts,
            costCentres,
            financeAreas
          );

          bulkUploadContract.validationErrorMessages = bulkUploadValidationErrors;

          bulkUploadContractsResponses[
            bulkUploadContract.gridRowNumber
          ] = bulkUploadValidationErrors;
        });

        currentUploadCacheValues.validatedBulkUploadContracts.addItems(
          bulkUploadContracts.items
        );

        this.bulkUploadCache.set(
          bulkUploadContracts.batchGuid,
          currentUploadCacheValues
        );

        if (bulkUploadContracts.isEndOfCurrentBatch) {
          if (
            !currentUploadCacheValues.validatedBulkUploadContracts
              .validationErrorCount
          ) {
            const sequelizeTransaction = await SequelizeUtil.databaseConnection.transaction();
            try {
              const createContractsPromises = currentUploadCacheValues.validatedBulkUploadContracts.items.map(
                (bulkUploadContract: IgfmDtoModel.BulkUploadContract) => {
                  const newContract: IgfmDtoModel.Contract = new IgfmDtoModel.Contract(
                    undefined,
                    bulkUploadContract.billableItemID,
                    bulkUploadContract.providerFinanceAreaID,
                    bulkUploadContract.providerProfitCentreID,
                    undefined,
                    bulkUploadContract.recipientBusinessUnitHighlevelName.trim()
                      ? bulkUploadContract.recipientBusinessUnitID
                      : null,
                    bulkUploadContract.recipientCompanyID,
                    bulkUploadContract.recipientCostCentreID,
                    undefined,
                    bulkUploadContract.recipientFinanceAreaID,
                    moment(bulkUploadContract.startDate, 'YYYY-MM-DD').toDate(),
                    moment(
                      bulkUploadContract.terminationDate,
                      'YYYY-MM-DD'
                    ).toDate()
                  );
                  return SequelizeUtil.databaseConnection.models.Contract.create(
                    newContract,
                    { transaction: sequelizeTransaction }
                  );
                }
              );
              await Promise.all(createContractsPromises);
              await sequelizeTransaction.commit();
            } catch (error) {
              await sequelizeTransaction.rollback();
              throw new InternalServerError(error);
            }
          }
        }
        resolve(bulkUploadContractsResponses);
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getBillableItems(): Promise<IgfmDtoModel.BillableItems> {
    return new Promise<IgfmDtoModel.BillableItems>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const billableItems = (
          await SequelizeUtil.databaseConnection.models.BillableItem.findAll({
            include: [
              {
                as: 'BillableItemCategory',
                model:
                  SequelizeUtil.databaseConnection.models.BillableItemCategory
              },
              {
                as: 'ProviderFinanceArea',
                model:
                  SequelizeUtil.databaseConnection.models.ProviderFinanceArea
              }
            ]
          })
        ).map((billableItem) => billableItem.get({ plain: true }));

        resolve(new IgfmDtoModel.BillableItems(billableItems));
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getBusinessUnits(): Promise<IgfmDtoModel.BusinessUnits> {
    return new Promise<IgfmDtoModel.BusinessUnits>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const businessUnits = (
          await SequelizeUtil.databaseConnection.models.BusinessUnit.findAll()
        ).map((businessUnit) => businessUnit.get({ plain: true }));
        resolve(new IgfmDtoModel.BusinessUnits(businessUnits));
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getCompanies(): Promise<IgfmDtoModel.Companies> {
    return new Promise<IgfmDtoModel.Companies>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const companies = (
          await SequelizeUtil.databaseConnection.models.Company.findAll()
        ).map((company) => company.get({ plain: true }));
        resolve(new IgfmDtoModel.Companies(companies));
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getContracts(): Promise<IgfmDtoModel.Contracts> {
    return new Promise<IgfmDtoModel.Contracts>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const contracts = (
          await SequelizeUtil.databaseConnection.models.Contract.findAll({
            include: [
              {
                as: 'BillableItem',
                model: SequelizeUtil.databaseConnection.models.BillableItem
              },
              {
                as: 'ProviderFinanceArea',
                model:
                  SequelizeUtil.databaseConnection.models.ProviderFinanceArea
              },
              {
                as: 'ProviderProfitCentre',
                model: SequelizeUtil.databaseConnection.models.CostCentre
              },
              {
                as: 'RecipientBusinessUnit',
                model: SequelizeUtil.databaseConnection.models.BusinessUnit
              },
              {
                as: 'RecipientCompany',
                model: SequelizeUtil.databaseConnection.models.Company
              },
              {
                as: 'RecipientCostCentre',
                model: SequelizeUtil.databaseConnection.models.CostCentre
              },
              {
                as: 'RecipientFinanceArea',
                model:
                  SequelizeUtil.databaseConnection.models.ProviderFinanceArea
              }
            ]
          })
        ).map((contract) => contract.get({ plain: true }));
        resolve(new IgfmDtoModel.Contracts(contracts));
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getCostCentres(): Promise<IgfmDtoModel.CostCentres> {
    return new Promise<IgfmDtoModel.CostCentres>(async (resolve, reject) => {
      try {
        await SequelizeUtil.connect();
        SequelizeUtil.define(IgfmDbModel);
        const costCentres = (
          await SequelizeUtil.databaseConnection.models.CostCentre.findAll()
        ).map((costCentre) => costCentre.get({ plain: true }));
        resolve(new IgfmDtoModel.CostCentres(costCentres));
      } catch (error) {
        reject(new InternalServerError(error));
      }
    });
  }

  private async getFinanceAreas(): Promise<IgfmDtoModel.ProviderFinanceAreas> {
    return new Promise<IgfmDtoModel.ProviderFinanceAreas>(
      async (resolve, reject) => {
        try {
          await SequelizeUtil.connect();
          SequelizeUtil.define(IgfmDbModel);
          const providerFinanceAreas = (
            await SequelizeUtil.databaseConnection.models.ProviderFinanceArea.findAll()
          ).map((providerFinanceArea) =>
            providerFinanceArea.get({ plain: true })
          );
          resolve(new IgfmDtoModel.ProviderFinanceAreas(providerFinanceAreas));
        } catch (error) {
          reject(new InternalServerError(error));
        }
      }
    );
  }
}
